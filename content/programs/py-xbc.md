---
title: py-xbc
---
py-xbc is a Python library for reading and writing documents written in the 'eXtra Boot Configuration' format used by the Linux kernel. It can be found on [PyPI] and the [Treehouse Gitea].

[PyPI]: https://pypi.org/project/py-xbc/
[Treehouse Gitea]: https://gitea.treehouse.systems/VulpineAmethyst/py-xbc