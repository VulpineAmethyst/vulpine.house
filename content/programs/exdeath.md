---
title: Exdeath
---
Exdeath is a program for patching Final Fantasy V Advance. Its source may be found [on the Treehouse Gitea](https://gitea.treehouse.systems/VulpineAmethyst/Exdeath). You will need a ROM image of the US version of the game, which you can find through your favourite search engine.

## Main

The real meat of the program: the patches.
* **Base**:
  * None -- the base game.
  * Waddle rebalance -- A remixed version of the game which aims to pump up underwhelming Jobs without altering overall balance too much.
  * [Balance](https://www.dropbox.com/s/rvf9l3pwbkelu42/ffvamod-doc.txt?dl=0) -- A remixed version by wormsofcan which adds the Hero Job and changes a bunch of other things.
  * [Custom Classes](http://jeffludwig.com/ff5a/download.php) -- A heavily remixed version by ludmeister.
* **Unlocked Jobs**: Unlocks all jobs directly out of the gate. Half of [wormsofcan's Fiesta patch](https://www.dropbox.com/s/ldlmpoepxk5nxgl/fiesta.ups?dl=0).
* **FFT-style Portraits**: As the name implies, this option replaces the portraits with edited Final Fantasy Tactics portraits. (missing attribution, sorry.)
* **Sound Restoration**: The [Sound Restoration](http://www.romhacking.net/hacks/563/) hack modifies the soundfont and corrects some slow-down issues. It can cause artifacting, however. Older versions of VisualBoyAdvance will require the GBA BIOS file in order to run normally with this option.
* **Save Config**: Saves your current configuration, including the ROM location. This configuration will be automatically loaded on next run.

## Randomization

Randomizable components.

* **Seed**: A number used to set the random number generator to a known state. The range accepted is 0 through 2**31-1, inclusive. Default value is the current time in seconds since 1 January 1970 00:00 UTC, divided by 3600.
* **Neo ExDeath**: Pick graphics for the final boss.
* **Abilities**: Shuffles abilities such that all Jobs learn five abilities and have a randomized command.

## Innate abilities

Each checkbox applies the listed innate ability to all Jobs.

## Multipliers

Multiply experience, ability point, or gil awards from battle. Experience and gil are hard-capped at 65,535 and ability points are hard-capped at 255, regardless of multiplier.

## Notes

The 'Unlocked Jobs' mode includes a patch that allows **!Combine** ammo to drop prior to unlocking the sealed weapons. This alters the drop tables for the following enemies:

* **Buckshot**:
  * Black Flame
  * Birostris
  * Elm Gigas
* **Blastshot**:
  * Blood Slime
  * Executor
* **Blitzshot**:
  * Mummy
