---
title: Smeargle
---
Smeargle is a program for rendering text as graphics using a bitmap font. It presently comes in two flavors:

* [Python](https://github.com/VulpineAmethyst/smeargle.py) -- the original implementation, built on top of PyQt5
* [C#](https://github.com/VulpineAmethyst/Smeargle)

Both flavours provide similar functionality; however, the Python version uses JSON for configuration and the C# version uses TOML for configuration.

## Features

- deduplication (optional)
- configurable tilemap output (compatible with Thingy32 and Atlas/Cartographer)
- support for monospace/fixed-width and variable-width bitmap fonts
- PNG inputs and outputs
- output limits (minimum width, maximum width)
- Unicode-based glyph mapping
- palette specification (Python-specific, required by PyQt if you want a fixed palette order)
