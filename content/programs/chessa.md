---
title: Chessa
---

This page documents Chessa's commands. Her source can be found on [the Adélie Linux GitLab](https://git.adelielinux.org/Sheila/Chessa).

## Dicebot

All dicebot commands support four special features not available to other commands:

* `.help` gives usage information and a description of the command.
* `.<command>-as <nick>` processes the command as though `nick` had used it.
* `.<command>-to <nick>` sends the output of the command to you and `nick` as a private message.
* `.<command> ; <comment>` appends the comment to the end of the command's output.

The following commands are associated:

* `.attrib` generates D&D dice-rolled attributes.
* `.silcore` rolls dice for Silhouette Core.
* `.exalted` rolls dice for Exalted 2e; `.exalted <dice> 6` may be used for old World of Darkness.
* `.hellcats` rolls dice for Hellcats & Hockeysticks.
* `.fudge` rolls dice for Fudge and Fate, and supports the Fate ladder.
* `.lonewolf` rolls a die for the Lone Wolf gamebooks and tabletop game.
* `.dryh` rolls dice for Don't Rest Your Head.
* `.dw` rolls dice for Dungeon World and variants.
* `.swrpg` (and aliases `.eote`, `.aor`, and `.fad`) rolls dice for the Star Wars RPG tabletop game.
* `.pfbuy` reports the point cost of the given Pathfinder array.
* `.explode` rolls exploding dice.
* `.cortex` rolls the requested dice per Cortex Prime rules.
* `.imperialassault` (and `.ia` alias) rolls coloured dice per the Imperial Assault board game.
* `.blade` rolls the requested number of d6's per Forged in the Dark rules.
* `.roll` and `.math` are more generic; the former allows for multiple formulas to be calculated at once, whereas the latter allows spaces to be used to make formulas more legible.

The `.roll` and `.math` commands support the following:

* PEMDAS (exponents use `**` rather than `^`)
* Bitwise operators (`&|^~`)
* d operator (as in `1d6`), rolls a number of X-sided dice, up to 100d100
* functions
  * `int` (truncates, converts hexadecimal, octal, and binary integers to decimal integers)
  * `hex` (converts decimal, octal, and binary integers to hexadecimal)
  * `oct` (converts decimal, hexadecimal, and binary integers to octal)
  * `bin` (converts hexadecimal, decimal, and octal integers to binary)
  * `atan2` (two-argument arc-tangents)
  * `cos` (cosine)
  * `exp` (e to the nth power)
  * `log` (logarithm of n)
  * `logn` (logarithm of n (base x))
  * `log10` (logarithm of n (base 10))
  * `sin` (sine)
  * `asin` (arcsine)
  * `sqrt` (Square root)
  * `c2f` (convert Celsius to Fahrenheit)
  * `f2c` (convert Fahrenheit to Celsius)
  * `floor` (round float downward)
  * `ceil` (round float upward)
  * `min` (smallest of given arguments)
  * `max` (largest of given arguments)
* variables
  * pi
  * e
  * pie (pi * e)

## Weather

The weather command set consists of `.weather`, `.f`/`.f1`/`.f3`, `.alerts`, and `.location`. All commands take a query string and, optionally, a semicolon (`;`) followed by a country designator. This designator will constrain the query to the given country, in cases where an ambiguous query may return an unexpected result (e.g. `.weather hell` might return the weather for Hellin, Spain, whereas `.weather hell;no` will return the weather for Hell, Norway).

`.f` returns today's forecast, `.f1` returns tomorrow's forecast, and `.f3` returns (via PM) the forecast for the three days following today. Note that the forecast will change over at 00:00 local time.

`.location` stores the provided location for the user, so as to allow future inquiries to omit a query string.

## Issue tracker

Chessa integrates with Adélie's GitLab and with Github. Supported syntax: `source:project/repo?n`, where

* `source:` is either `adelie:`, or `github:` or `gh:`,
* `project` is the organization or user where the repo can be found,
* `repo` is the repository in question,
* `?` is one of the sigils in `$#@!`,
* and `n` is a number (for `$#!`) or commit (for `@`).

Sigils have their GitLab meaning:

* `$` is a snippet
* `@` is a commit
* `#` is an issue
* `!` is a merge request

Portions of the syntax may be dropped, e.g. at the moment she is configured to default to `adelie/packages`.

**Note**: This module is a work in progress.

## Other Commands

* `.choose` returns a randomly-chosen entry from a list, delimited by a comma followed by a space.
* `.unicode` returns information on the given Unicode sequence. Supports combining characters.
* `.uniname` returns information on the given Unicode named sequence.

