---
title: Home
description: A deafblind, autistic/ADHD fox system's den.
---
Welcome! This site is a handy collection of links to my various accounts, as well as information about us and our endeavors.

We are primarily non-speaking; we prefer to communicate in text unless verbal communication is required. We are deafblind, which is maintained in all of our facets. We wear hearing aids, and we are a power chair user. If we need to walk, which we are capable of doing for short distances, we navigate with a white cane.
