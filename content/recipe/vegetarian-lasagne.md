---
title: Vegetarian Lasagne
tags:
- main
- kosher
- dairy
- pareve
---
*Ingredients*:
- 1 (16 ounce) carton ricotta cheese, part-skim recommended
- 1 egg
- 1/4 cup grated Parmesan cheese
- 1/4 teaspoon garlic powder OR 1/2 teaspoon jarred garlic
- 1 (10 ounce) box frozen chopped spinach, thawed and squeezed dry
- 1 jar spaghetti sauce + 1/4 cup white wine to rinse it
- 9 uncooked lasagne noodles
- 3 cups shredded part-skim mozzarella cheese

*Directions*:

#. Preheat the oven to 375 degrees.
#. Pour sauce into medium bowl, then rinse jar with wine. Stir to combine.
#. In another medium bowl, combine ricotta, egg, Parmesan, garlic powder and spinach. Mix well.
#. Spread one third of the spaghetti sauce in the bottom of a 11" x 7" brownie pan.
#. Cover with 3 uncooked lasagna noodles. Top with half the ricotta mixture and then sprinkle with 1 cup mozzarella.
#. Spread with half the remaining spaghetti sauce.
#. Top with another 3 noodles. Spread the rest of the ricotta mixture over the noodles and sprinkle with 1 cup mozzarella.
#. Top with the last three noodles, then spread on the rest of the spaghetti sauce. Sprinkle the rest of the mozzarella on top.
#. Cover with foil sprayed with cooking spray.
#. Bake 45-50 minutes or until noodles are tender.
#. Let stand 10 minutes before serving.

