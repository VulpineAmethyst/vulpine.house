---
title: Chicken and Potatoes
tags:
- chicken
- main
- instant pot
- kosher
---
- 2 lbs chicken, any format
- 3 lbs potatoes, cubed
- 1 large onion, chopped
- 1 fresh rosemary sprig or 1 tbsp dried
- 3 bay leaves
- 1 1/2 tsp salt
- Ground black pepper to taste
- 1 cup water
- 1 garlic clove grated
- 1 small bunch dill, Italian parsley, or green onion, finely chopped

1. In Instant Pot, add chicken, potatoes, onion, carrots, rosemary, bay leaves, salt, pepper and water. Do not stir.
2. Close the lid, turn pressure vent to Sealing and press Pressure Cook on High or Manual for 15 minutes for boneless chicken and 20 minutes for bone-in chicken pieces.
3. After Instant Pot has finished cooking, release pressure using Quick Release method by turning valve to Venting and open the lid.
4. Add garlic and fresh herbs. Stir gently. I like to separate meat off the bone at same time. It helps to divide meat more evenly among the family members.
5. Serve warm.
