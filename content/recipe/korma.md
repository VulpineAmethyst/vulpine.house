---
title: Chicken korma
description: A mild coconut curry chicken dish by way of India.
tags:
- main dish
- dairy
- chicken
- trayf
---
*Ingredients*:
- 1 whole chicken cut into legs, thighs, and breasts
- 1 can coconut cream
- 1 red onion, finely diced
- 1 cup chicken stock
- 1 cup full fat yogourt
- 1/4 cup unsweetened shredded coconut
- 2 tbsp canola oil
- 1 tbsp tomato paste
- 1 tbsp crushed garlic
- 1 tbsp crushed ginger
- 1 tbsp Garam masala
- 1 tbsp kashmiri chili powder
- 1 tsp ground cumin
- 1 tsp ground coriander
- 1 tsp ground clove
- 1/2 tsp ground cinnamon
- 1/2 tsp kosher salt
- 1/4 tsp black pepper

*Garnish ingredients*:
- 1/4 cup heavy cream
- 1 lime, cut into wedges

*Directions*:
1. Marinade the chicken in yogourt, garlic and ginger in the fridge covered for 8 to 24 hours
2. In a large saute pan, saute onions until translucent, adding the tomato paste and cooking until fragrant
3. Add your chicken to the pan and thoroughly brown, adding your spices, coconut, coconut cream and chicken stock and bringing to simmer.
4. Simmer on medium low until the chicken is spoon tender, about an hour.
5. Garnish finished korma with a dash of cream and a side of lime and serve with basmati rice and naan.
