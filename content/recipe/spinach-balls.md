---
title: Spinach Balls
tags:
- dairy
- kosher
- vegetarian
---
*Ingredients*:
- 2 packages frozen chopped spinach, thawed and squeezed dry
- 4 beaten eggs
- 3/4 c. melted butter
- 1/2 c. grated Parmesan cheese
- 1/2 tsp. salt
- 1 onion, finely chopped
- 2 c. herb stuffing mix

*Directions*:
Mix together and chill 15 minutes. Shape into balls. Put on greased cookie sheet, bake 15 minutes at 375. 
