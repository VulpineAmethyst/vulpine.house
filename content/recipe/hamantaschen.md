---
title: Blueberry-Lemon Hamantaschen
tags:
- dessert
- kosher
- jewish
---
*Ingredients* (filling):
- 4 cups frozen blueberries
- 1/2 cup sugar
- 1 tablespoon cornstarch or arrowroot powder

*Ingredients* (hamantaschen):
- 1/2 cup extra-virgin olive oil
- 1/2 cup granulated sugar
- 2 eggs
- 1/2 teaspoon lemon juice
- 1 teaspoon lemon zest
- 2 1/2 cups all-purpose flour
- 2 1/2 teaspoons baking powder
- 1 pinch kosher salt

*Directions*:

For the filling:

1. Place frozen blueberries in a medium saucepain and bring to a boil.
2. Add sugar and stir until dissolved.
3. Reduce to a rapid simmer and allow mixture to thicken for 3 to 5 minutes.
4. In a small bowl or cup, dilute starch with a little bit of water; stir to dissolve.
5. Add starch slurry to blueberries.
6. Remove from heat and cool.

For the hamantaschen:

Preheat oven to 375 degrees Fahrenheit. Line 2 cookie sheets with parchment paper.

1. In a large bowl, mix olive oil with sugar, eggs, lemon juice, and lemon zest.
2. Add flour, baking powder, and salt, and mix gently until dough forms.
3. Lightly dust parchment paper with flour. Divide dough in half and roll it out to 1/8th-inch thick. Cut out 3-inch rounds.
4. Fill with one teaspoon of your favourite filling. Fold the left side over the filling in to the center, then the right side, and finally the bottom side, making a triangle.
5. Place cookies on prepared sheets and bake for about 15 minutes, until golden brown.
