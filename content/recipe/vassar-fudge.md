---
draft: true
title: Vassar Fudge
description: A simple fudge that's kosher for Passover.
tags:
- dessert
- kosher
- kosher for passover
---
*Ingredients*:
- 2 cups sugar
- 1 cup light cream
- 2 squares (2 ounces) unsweetened chocolate, coarsely chopped
- 1 tablespoon butter

*Directions*:
#. Combine sugar, chocolate, and cream.
#. Cook over a moderate heat, stirring only until sugar and chocolate have melted.
#. Continue cooking until mixture reaches 238F, or until a few drops tested in cold water form a soft ball.
#. Remove from heat, add butter, and cool slightly.
#. Beat until fudge begins to harden, then transfer to a buttered platter.
#. Cut into squares before the fudge is absolutely firm. Makes a little more than one pound.

