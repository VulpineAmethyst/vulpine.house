---
title: Slow Cooker Potato Soup
tags:
- dairy
- soup
---
- 32oz frozen hash browns
- imitation bacon crumbles
- 32oz vegetable broth
- 1 can (10.5oz) cream of mushroom soup
- 1.5 cups shredded cheddar
- 1 cup heavy cream or milk

Combine hashbrowns, bacon crumbles, broth, and cream of mushroom soup in a slow cooker. Cook on low 5-6 hours. Half an hour before completion, add cheese and heavy cream and mix until well-incorporated.
