---
title: Translations
---
This is a collection of translations I’ve done over the years. Some of them are incomplete. Some may contain errors. Contributions and feedback are welcome.

Special thanks to [Sky Render](https://www.skyrender.net) for her invaluable assistance on many of these translations.

## Translation Notes

There are some recurring terms where my translations will raise, and have raised, eyebrows, so I will address them here, and explain my reasoning.

- **Mógli**: This seems the most reasonable compromise between calling them 'molebears', which they don't really resemble in most incarnations at this juncture, and 'Moogle', the official translation. For a while I'd chosen 'Mowgli', as a nod to the Rudyard Kipling character, but that was contentious...and anyway, Kipling's a racist coloniser.
- **Mortboule**: From French 'mort' (death) and 'boule' (ball), as most incarnations of this monster quite deadly and are tentacular ball horrors. It also sounds more or less like how native English speakers say 'morbol', a more literal transliteration that has been found in a few official translations.
- **Tombury**: Traditional Hepburn romanization turns the n before b and p in to m, thus 'sempai' and 'kempo', and this practice, when reversed, yields 'zonbī' from 'zombie'. More than that, this monster is quite deadly.
- **Curera, etc.**: In the original Famicom version of Final Fantasy I, spells were limited to four characters in length, for example _FU-a-I-A_ (Fire). We can tell from both the Cure (_kearu_) line in this game and the Aero (_earo_) line in future games that the intended reading was for _-ra_, _-ga_, etc. to be a suffix, not erase part of the spell name entirely as the official translation does. Thus, Firega, Cureja, etc.
- **Null Status**: The _-na_ line of spells nullifies status conditions; Poisona nullifies Poison, for example. If one abbreviates 'status' to a single letter and adds _-na_, one gets 'Esuna', thus 'Null Status'.

## Translations
