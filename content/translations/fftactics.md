---
title: Final Fantasy Tactics
translation: true
draft: true
---

I'm not a huge fan of strategy RPGs as a genre (for many reasons, which I won't discuss here), but the Final Fantasy Tactics series is a bit unusual in that I _want_ to like it (at least after FFT), but can't for _some_ of those aforementioned reasons.

That said, translation issues are way up there, hence this guide.

## People

| Japanese                  | Romaji                 | Síle                   | PlayStation          | War of the Lions     |
| ------------------------- | ---------------------- | ---------------------- | -------------------- | -------------------- |
| ラムザ ベオルブ           | ramuzabeorubu          | Ramza Béolve           | Ramza Beoulve        | Ramza Beoulve        |
| ディリータ ハイラル       | dirītahairaru          | Delita Hyrule[^1]      | Delita Hyral         | Delita Heiral        |
| オヴェリア アトカーシャ   | overiaatokāsha         | Ovelia Atkarsha        | Ovelia Atkascha      | Ovelia Atkascha      |
| アルマ ベオルブ           | arumabeorubu           | Alma Béolve            | Alma Beoulve         | Alma Beoulve         |
| ザルバッグ ベオルブ       | zarubaggubeorubu       | Zalbag Béolve          | Zalbag Beoulve       | Zalbaag Beoulve      |
| ダイスダーグ ベオルブ     | daisudāgubeorubu       | Dycedarg Béolve        | Dycedarg Beoulve     | Dycedarg Beoulve     |
| ベストラルダ ラーグ       | besutorarudarāgu       | Bestrald Larg          | Bestrada Larg        | Bestrald Larg        |
| ダクスマルダ ゴルターナ   | dakusumarudagorutāna   | Daxmaldan Gortana      | Druksmald Goltana    | Druksmald Goltanna   |
| オリナス アトカーシャ     | orinasuatokāsha        | Orinas Atkarsha        | Orinas Atkascha      | Orinus Atkascha      |
| ルーヴェリア アトカーシャ | rūveriaatokāsha        | Rulvelia Atkarsha      | Ruvelia Atkascha     | Louveria Atkascha    |
| オムドリア アトカーシャ   | omudoriaatokāsha       | Omdolian Atkarsha      | Omdoria Atkascha     | Ondoria Atkascha     |
| マリッジ フューネラル     | marijjifyūneraru       | Mariz Funeral          | Marge Funeral        | Marcel Funebris      |
| アルガス サダルファス     | arugasusadarufasu      | Argus Thadalfus        | Algus Sadalfas       | Argath Thadalfus     |
| ガフ ガフガリオン         | gafugafugarion         | Gaff Gafgarion         | Gaff Gafgarion       | Goffard Gaffgarion   |
| アグリアス オークス       | aguriasuōkusu          | Agrias Oaks            | Agrias Oaks          | Agrias Oaks          |
| シドルファス オルランドゥ | shidorufasuorurandu    | Cidolphus Orlandeau    | Cidolfas Orlandu     | Cidolfus Orlandeau   |
| オーラン デュライ         | ōrandurai              | Olran Durai            | Olan Durai           | Orran Durai          |
| ザルモゥ ルスナーダ       | zarumurusunāda         | Zalum Rusnalda         | Zalmo Rusnada        | Zalmour Lucianada    |
| ブレモンダ フリートベルク | buremondafurītoberuku  | Bremondan Friet-Beruch | Buremonda            | Bremondt Freitberg   |
| シモン ペン ラキシュ      | shimonpenrakishu       | Simon Penn Lakishu     | Simon Pen Rakshu     | Simon Penn-Lachish   |
| アーレス ローゼンハイム   | āresurōzenhaimu        | Ares Rosenheim         | (not present)        | Aliste Rosenheim     |
| ベイオウーフ カドモス     | beioūfukadomosu        | Beowulf Cadmos         | Beowulf Kadmas       | Beowulf Cadmus       |
| ウィーグラフ フォルズ     | wīgurafuforuzu         | Wiglaf Forza           | Wiegraf Folles       | Wiegraf Folles       |
| レーゼ デューラー         | rēzedūrā               | Reis Duelral           | Reis Dular           | Reis Duelar          |
| バルマウフラ ラナンドゥ   | barumaufuraranandu     | Barmaufra Lanandu      | Balmafula Lanando    | Valmafra Lenande     |
| アルフォンス ドラクロワ   | arufonsudorakurowa     | Alphonse Delacroix     | Alphons Draclau      | Alphonse Delacroix   |
| ラファ ガルテナーハ       | rafagarutenāha         | Rapha Galtenarha       | Rafa Galthana        | Rapha Galthena       |
| マラーク ガルテナーハ     | marākugarutenāha       | Malach Galtenarha      | Malak Galthana       | Marach Galthena      |
| メスドラーマ エルムドア   | mesudorāmaerumudoa     | Mesdralma Elmdor       | Mesdoram Elmdor      | Messam Elmdore       |
| ゲルカラニス バリンテン   | gerukaranisubarinten   | Gelkaranis Barrington  | Gelkanis Barinten    | Gerrith Barrington   |
| ムスタディオ ブナンザ     | musutadiobunanza       | Mustadio Bunanza       | Mustadio Bunansa     | Mustadio Bunansa     |
| ベスロディオ ブナンザ     | besurodiobunanza       | Besrodio Bunanza       | Besrodio Bunanza     | Besrudio Bunansa     |
| バート ルードヴィッヒ     | bātorūdovihhi          | Bart Ludovici          | Rudvich Bart         | Ludovich Baert       |
| セリア                    | seria                  | Celia                  | Celia                | Celia                |
| レディ                    | redi                   | Lede                   | Lede                 | Lettie               |
| アジョラ グレバドス       | ajoragurebadosu        | Ajora Glebados         | Ajora Glabados       | Ajora Glabados       |
| ヴォルマルフ ティンジェル | vorumarufutinjeru      | Volmarf Tingelle       | Vormav Tingel        | Folmarv Tengille     |
| ローファル ウォドリング   | rōfaruwodoringu        | Rolfal Wodring         | Rofel Wodring        | Loffrey Wodring      |
| イズルード ティンジェル   | izurūdotinjeru         | Isolde Tingelle        | Izlude Tingel        | Isilud Tengille      |
| クレティアン ドロワ       | kuretiandorowa         | Chrétien Droix         | Kletian Drowa        | Cletienne Duroi      |
| バルク フェンゾル         | barukufenzoru          | Baruch Fendsor         | Balk Fenzol          | Barich Fendsor       |
| メリアドール ティンジェル | meriadōrutinjeru       | Meriador Tingelle      | Meliadoul Tingel     | Meliadoul Tengille   |
| クラウド ストライフ       | kuraudosutoraifu       | Cloud Strife           | Cloud Strife         | Cloud Strife         |
| ティータ ハイラル         | tītahairaru            | Tirta Hyrule[^1]       | Teta Hyral           | Tietra Heiral        |
| バルバネス・ベオルブ      | barubanesubeorubu      | Valbanes Béolve        | Balbanes Beoulve     | Barbaneth Beoulve    |
| ギュスタヴ マルゲリフ     | gyusutavumarugerifu    | Gustav Malgérif        | Gustav Margueriff    | Gustav Margriff      |
| ミルウーダ フォルズ       | miruūdaforuzu          | Mirulda Forza          | Miluda Folles        | Milleuda Folles      |
| ゴラグロス ラヴェイン     | goragurosuravein       | Gragoroth Levigne      | Golagros Levine      | Gragoroth Levigne    |
| ルッソ                    | russo                  | Luso                   | (not present)        | Luso                 |
| バルフレア                | barufurea              | Balflare               | (not present)        | Balthier             |
| ボーアダム ダーラボン     | bōadamudarabon         | Boradam Daravon        | Bordam Daravon       | Master Darlavon      |
| アラズラム Ｊ･デュライ    | arazuramuJdurai        | Alazram J. Durai       | Alazlam Durai        | Arazlam Durai        |
| ラッド                    | raddo                  | Radd                   | Rad                  | Ladd                 |
| アリシア                  | arishia                | Alicia                 | Alicia               | Alicia               |
| ラヴィアン                | ravian                 | Lavian                 | Lavian               | Lavian               |
| 労働八号                  | rondō8gō               | Labourer No. 8         | Worker-8             | Construct 8          |
| 労働七号・改              | rondō7gō.kai           | Labourer No. 7+        | Worker-7-New         | Construct 7          |
| グルワンヌ大臣            | guruwannudaijin        | Minister Gruwan        | Minister Gelwan      | Chancellor Glevanne  |
| エアリス                  | earisu                 | Aerith                 | Aeris                | Aerith               |
| ゲルモニーク              | gerumonīku             | Gelmonique             | Germonik             | Germonique           |
| レザレス                  | rezaresu               | Lezales                | Lezales              | Lezalas              |
| ?                         | ?                      | Baron Grimms           | Baron Grims          | Baron Grimms         |
| 司教カンバベリフ          | shikyōkanbaberifu      | Bishop Canba-Berif     | Kanbabrif            | Bishop Canne-Beurich |
| ボルミナ男爵              | boruminadanshaku       | Baron Bolmina          | Bolmna               | The Baron of Bolmina |
| ブランシュ子爵            | buranshushishaku       | Viscount Blanche       | Blansh               | Viscount Blanche     |
| シドに化けた グレバドス教信者 | shidonibaketa gurebadosukyōshinja | Corrupt Glebados Devout | Grevados             | Glabados Devout      |

[^1]: It's probably not an intentional reference, but [see the Legend of Zelda: Ocarina of Time TAS](http://www.youtube.com/watch?v=AVBqIeFqxbI#t=394s).

## Jobs

| Japanese         | Romaji         | Síle               | PlayStation    | War of the Lions   |
| ---------------- | -------------- | ------------------ | -------------- | ------------------ |
| 見習い戦士       | minaraisenshi  | Warrior Apprentice | Squire         | Squire             |
| アイテム士       | aitemushi      | Chemist            | Chemist        | Chemist            |
| ナイト           | naito          | Knight             | Knight         | Knight             |
| 弓使い           | yumitukai      | Archer             | Archer         | Archer             |
| モンク           | monku          | Monk               | Monk           | Monk               |
| 白魔道士         | shiromadōshi   | White Mage         | Priest         | White Mage         |
| 黒魔道士         | kuromadōshi    | Black Mage         | Wizard         | Black Mage         |
| 時魔道士         | jimadōshi      | Time Mage          | Time Mage      | Time Mage          |
| 召喚士           | shōkanshi      | Summoner           | Summoner       | Summoner           |
| シーフ           | shīfu          | Thief              | Thief          | Thief              |
| 話術士           | wajutsushi     | Diplomat           | Mediator       | Orator             |
| 陰陽士           | onmyōshi       | Prophet            | Oracle         | Mystic             |
| 風水士           | fūsuishi       | Geomancer          | Geomancer      | Geomancer          |
| 竜騎士           | ryūkishi       | Dragon Knight      | Lancer         | Dragoon            |
| 侍               | samurai        | Samurai            | Samurai        | Samurai            |
| 忍者             | ninja          | Ninja              | Ninja          | Ninja              |
| 算術士           | sanjutsushi    | Mathemagician      | Calculator     | Arithmetician      |
| 吟遊詩人         | ginyūshijin    | Bard               | Bard           | Bard               |
| 踊り子           | odoriko        | Dancer             | Dancer         | Dancer             |
| 機工士           | kikōshi        | Engineer           | Engineer       | Machinist          |
| ホーリーナイト   | hōrīnaito      | Holy Knight        | Holy Knight    | Holy Knight        |
| 天道士           | tendōshi       | Celestial Knight   | Heaven Knight  | Skyseer            |
| 剣聖             | kensei         | Swordsaint         | Holy Swordsman | Sword Saint        |
| ディバインナイト | dibainnaito    | Divine Knight      | Divine Knight  | Divine Knight      |
| ダークナイト     | dākunaito      | Dark Knight        | Dark Knight    | Fell Knight        |
| テンプルナイト   | tenpurunaito   | Temple Knight      | Temple Knight  | Templar            |
| ドラグナー       | doragunā       | Dragonborn         | Dragoner       | Dragonkin          |
| ソルジャー       | sorujā         | Soldier            | Soldier        | Soldier            |
| 占星術士         | senseijutsushi | Astrologer         | Astrologist    | Astrologer         |
| クレリック       | kurerikku      | Cleric             | Cleric         | Cleric             |
| ホワイトナイト   | howaitonaito   | White Knight       | White Knight   | White Knight       |
| プリンセス       | purinsesu      | Princess           | Princess       | Princess           |
| ハイプリースト   | haipurīsuto    | High Priest        | Holy Priest    | Celebrant          |
| ナイトブレード   | naitoburēdo    | Nightblade         | Knight Blade   | Nightblade         |
| 天冥士           | tenmeishi      | Infernal Knight    | Hell Knight    | Netherseer         |
| アークナイト	   | ākunaito       | Ark Knight         | Arc Knight     | Ark Knight         |
| アサシン         | asashin        | Assassin           | Assassin       | Assassin           |
| ソーサラー       | sōsarā         | Sorcerer           | Sorceror       | Sorcerer           |
| ルーンナイト     | rūnnaito       | Rune Knight        | Lune Knight    | Rune Knight        |
| 暗黒騎士         | ankokukishi    | Black Knight       | (not present)  | Dark Knight        |
| たまねぎ剣士     | tamanegikenshi | Onion Swordsman    | (not present)  | Onion Knight       |
| 空賊             | kūzoku         | Aerial Pirate      | (not present)  | Sky Pirate         |
| モブハンター     | mobuhantā      | Mob Hunter         | (not present)  | Game Hunter        |
| デスナイト       | desunaito      | Death Knight       | (not present)  | Deathknight        |

### Action Commands

| Japanese   | Romaji        | Síle                | PlayStation    | War of the Lions   |
| ---------- | ------------- | ------------------- | -------------- | ------------------ |
| 基本技     | kihonwaza     | Basic Techniques    | Basic Skill    | Fundaments         |
| アイテム   | aitemu        | Item                | Item           | Items              |
| 戦技       | senwaza       | Combat Techniques   | Battle Skill   | Arts of War        |
| チャージ   | chāji         | Charge              | Charge         | Aim                |
| 拳術       | kenjutsu      | Martial Arts        | Punch Art      | Martial Arts       |
| 白魔法     | shiromahō     | White Magic         | White Magic    | White Magicks      |
| 黒魔法     | kuromahō      | Black Magic         | Black Magic    | Black Magicks      |
| 時魔法     | jimahō        | Time Magic          | Time Magic     | Time Magicks       |
| 召喚魔法   | shōkanmahō    | Summon Magic        | Summon Magic   | Summon             |
| 盗む       | nusumu        | Steal               | Steal          | Steal              |
| 話術       | wajutsu       | Diplomacy           | Talk Skill     | Speechcraft        |
| 陰陽術     | onmyōjutsu    | Prognostication     | Yin-Yang Magic | Mystic Arts        |
| 風水術     | fūsuijutsu    | Geomancy            | Elemental      | Geomancy           |
| ジャンプ   | janpu         | Jump                | Jump           | Jump               |
| 引き出す   | hikidasu      | Draw Out            | Draw Out       | Iaido              |
| 投げる     | nageru        | Throw               | Throw          | Throw              |
| 算術       | sanjutsu      | Mathemagic          | Math Skill     | Arithmeticks       |
| 詩う       | utau          | Sing                | Sing           | Sing               |
| 踊る       | odoru         | Dance               | Dance          | Dance              |
| ガッツ     | gattsu        | Guts                | Guts           | Mettle             |
| 狙撃       | sogeki        | Snipe               | Snipe          | Aimed Shot         |
| 聖剣技     | seikengi      | Holy Sword Arts     | Holy Sword     | Holy Sword         |
| 真言       | shingon       | Mantra              | Truth          | Sky Mantra         |
| 全剣技     | zenkengi      | Supreme Techniques  | All Swordskill | Swordplay          |
| 剛剣       | gōken         | Mighty Sword        | Mighty Sword   | Unyielding Blade   |
| 魔法剣     | mahōken       | Spellsword          | Magic Sword    | Spellblade         |
| ドラゴン   | doragon       | Dragon              | Dragon         | Dragon             |
| リミット   | rimitto       | Limit               | Limit          | Limit              |
| 星天       | seiten        | Heavenly Stars      | Starry Heaven  | Astrology          |
| 聖魔法     | seimahō       | Sacred Magic        | Holy Magic     | Holy Magicks       |
| 暗黒剣     | ankokuken     | Dark Sword          | Dark Sword     | Fell Sword         |
| 破壊魔剣   | hakaimaken    | Seal Sword          | Destroy Sword  | Blade of Ruin      |
| 吸血       | kyūketsu      | Bloodsucker         | Blood Suck     | Vampire            |
| 白養魔法   | shiroyōmahō   | White Support Magic | White-aid      | (not present)      |
| 白洛魔法   | shirorakumahō | White Support Magic | (not present)  | Priest Magicks     |
| 裏真言     | urashingon    | Mirror Mantra       | Un-Truth       | Nether Mantra      |
| 刀魂放気   | tōkonhōki     | Sword Spirit        | Sword Spirit   | Sword Spirit       |
| 仕手       | shite         | Performance         | Use Hand       | Subdual Arts       |
| 全魔法     | zenmahō       | Supreme Magic       | All Magic      | Magicks            |
| 剣技       | kengi         | Sword Techniques    | Sword Skill    | Swordplay          |
| 暗黒       | ankoku        | Shadow              | (not present)  | Darkness           |
| ターゲット | tāgetto       | Target              | (not present)  | Piracy             |
| エンゲージ | engēji        | Engage              | (not present)  | Huntcraft          |

## Abilities

### Action Abilities

These are organised by first appearance, based on the above command order.

| Japanese         | Romaji                | Síle                         | PlayStation       | War of the Lions    |
| ---------------- | --------------------- | ---------------------------- | ----------------- | ------------------- |
| ためる           | tameru                | Accumulate                   | Accumulate        | Focus               |
| 体当たり         | taiatari              | Ram                          | Dash              | Rush                |
| 投石             | tōseki                | Throw Rock                   | Throw Stone       | Stone               |
| 手当て           | teate                 | Treatment                    | Heal              | Salve               |
| ヘッドブレイク   | heddobureiku          | Break Head                   | Head Break        | Rend Helm           |
| アーマーブレイク | āmābureiku            | Break Armor                  | Armor Break       | Rend Armor          |
| シールドブレイク | shīrudobureiku        | Break Shield                 | Shield Break      | Rend Shield         |
| ウエポンブレイク | weponbureiku          | Break Weapon                 | Weapon Break      | Rend Weapon         |
| マジックブレイク | majikkubureiku        | Break Magic                  | Magic Break       | Rend MP             |
| スピードブレイク | supīdobureiku         | Break Speed                  | Speed Break       | Rend Speed          |
| パワーブレイク   | pawābureiku           | Break Power                  | Power Break       | Rend Power          |
| マインドブレイク | maindobureiku         | Break Mind                   | Mind Break        | Rend Magick         |
| チャージ+1       | chāji+1               | Charge +1                    | Charge +1         | Aim +1              |
| チャージ+2       | chāji+2               | Charge +2                    | Charge +2         | Aim +2              |
| チャージ+3       | chāji+3               | Charge +3                    | Charge +3         | Aim +3              |
| チャージ+4       | chāji+4               | Charge +4                    | Charge +4         | Aim +4              |
| チャージ+5       | chāji+5               | Charge +5                    | Charge +5         | Aim +5              |
| チャージ+7       | chāji+7               | Charge +7                    | Charge +7         | Aim +7              |
| チャージ+10      | chāji+10              | Charge +10                   | Charge +10        | Aim +10             |
| チャージ+20      | chāji+20              | Charge +20                   | Charge +20        | Aim +20             |
| 裏回し拳         | urakiwashiken         | Fielding Fist                | Spin Fist         | Cyclone             |
| 連続拳           | renzokuken            | Punch Rush                   | Repeating Fist    | Pummel              |
| 波動撃           | hadōgeki              | Wave Strike                  | Wave Fist         | Aurablast           |
| 地烈斬           | chiretsuki            | Earth Render                 | Earth Slash       | Shockwave           |
| 秘孔拳           | hikōken               | Pin-Point Strike             | Secret Fist       | Doom Fist           |
| 気孔術           | kikōjutsu             | Pore Technique               | Stigma Magic      | Purification        |
| チャクラ         | chakura               | Chakra                       | Chakra            | Chakra              |
| 蘇生             | sosei                 | Revive                       | Revive            | Revive              |
| ギル取り         | girutori              | Gil Snatcher                 | Steal Gil         | Steal Gil           |
| ハートを盗む     | hātoonusumu           | Steal Heart                  | Steal Heart       | Steal Heart         |
| 兜を盗む         | kabutoonusumu         | Steal Helm                   | Steal Helmet      | Steal Helmet        |
| 鎧を盗む         | yoroionusumu          | Steal Armor                  | Steal Armor       | Steal Armor         |
| 盾を盗む         | tateonusumu           | Steal Shield                 | Steal Shield      | Steal Shield        |
| 武器を盗む       | bukionusumu           | Steal Weapon                 | Steal Weapon      | Steal Weapon        |
| アクセサリを盗む | akusesarionusumu      | Steal Accessory              | Steal Accessory   | Stal Accessory      |
| Expを盗む        | Exponusumu            | Steal XP                     | Steal EXP         | Steal EXP           |
| 勧誘             | kanyū                 | Invite                       | Invitation        | Entice              |
| 説得             | settoku               | Persuade                     | Persuade          | Stall               |
| ほめる           | homeru                | Praise                       | Praise            | Praise              |
| おどす           | odosu                 | Threaten                     | Threaten          | Intimidate          |
| 説法             | sebbō                 | Lecture                      | Preach            | Preach              |
| 解法             | kaihō                 | Solution                     | Solution          | Enlighten           |
| 死の予言         | shinoyogen            | Predict Death                | Death Sentence    | Condemn             |
| 商談             | shōgen                | Negotiate                    | Negotiation       | Beg                 |
| 悪口             | waruguchi             | Slander                      | Insult            | Insult              |
| ダーラボンのまね | dārabonnomane         | Imitate Daravon              | Mimic Daravon     | Mimic Darlavon      |
| 闇縛符           | yamibakufu            | Darkness-Binding Sign        | Blind             | Umbra               |
| 魔吸唱           | makyūshō              | Spell-Absorbing Chant        | Spell Absorb      | Empowerment         |
| 命吸唱           | inochikyūshō          | Life-Absorbing Chant         | Life Drain        | Invigoration        |
| 信祈仰祷         | shinkigyōki           | Prayer for Faith             | Pray Faith        | Belief              |
| 信疑仰祷         | shingigyōki           | Prayer for Guidance          | Doubt Faith       | Disbelief           |
| 腐生骸屍         | fujōgaishi            | Vampiric Husk                | Zombie            | Corruption          |
| 沈黙唱           | chinmokushō           | Silence Chant                | Silence Song      | Quiescence          |
| 勇猛狂符         | yūmōkyōfu             | Bravery Charm                | Blind Rage        | Fervor              |
| 狐鶏鼠           | kokeiso               | Fox Chicken Mouse            | Foxbird           | Trepidation         |
| 乱心唱           | ranshin               | Madness                      | Confusion Song    | Delirium            |
| 絶装魔脱         | zessōmada             | Demons Unbound               | Dispel Magic      | Harmony             |
| 不変不動         | fuhenfudō             | Eternal Immobility           | Paralyze          | Hesitation          |
| 夢邪睡符         | yumeyokoshinemufu     | Slumbering Nightmare         | Sleep             | Repose              |
| 碑封印           | ishibumifūin          | Seal Memorial                | Petrify           | Induration          |
| 落とし穴         | otoshiana             | Pitfall                      | Pitfall           | Sinkhole            |
| 水塊             | suikai                | Deluge                       | Water Ball        | Torrent             |
| 蔦地獄           | tsutajigoku           | Hell Ivy                     | Hell's Ivy        | Tanglevine          |
| 彫塑             | chōso                 | Clay Model                   | Carve Model       | Contortion          |
| 局地地震         | kyokuchijishin        | Local Earthquake             | Local Quake       | Tremor              |
| かまいたち       | kamaitachi            | Razor Gale                   | Kamaitachi        | Wind Slash          |
| 鬼火             | onibi                 | Ignis Fatuus                 | Demon Fire        | Will-o'-the-Wisp    |
| 底なし沼         | sokonashinuma         | Boundless Bog                | Quicksand         | Quicksand           |
| 砂嵐             | sunaarashi            | Sandstorm                    | Sandstorm         | Sandstorm           |
| 吹雪             | fubuki                | Blizzard                     | Blizzard          | Snowstorm           |
| 突風             | toppū                 | Gust                         | Gusty Wind        | Wind Blast          |
| 溶岩ボール       | yōganbōru             | Lava Ball                    | Lava Ball         | Magma Surge         |
| 水平ジャンプ2    | suiheijanpu2          | Horizontal Jump 2            | Horizontal Jump 2 | Horizontal Jump 2   |
| 水平ジャンプ3    | suiheijanpu3          | Horizontal Jump 3            | Horizontal Jump 3 | Horizontal Jump 3   |
| 水平ジャンプ4    | suiheijanpu4          | Horizontal Jump 4            | Horizontal Jump 4 | Horizontal Jump 4   |
| 水平ジャンプ5    | suiheijanpu5          | Horizontal Jump 5            | Horizontal Jump 5 | Horizontal Jump 5   |
| 水平ジャンプ8    | suiheijanpu8          | Horizontal Jump 8            | Horizontal Jump 8 | Horizontal Jump 8   |
| 垂直ジャンプ2    | suichokujanpu2        | Vertical Jump 2              | Vertical Jump 2   | Vertical Jump 2     |
| 垂直ジャンプ3    | suichokujanpu3        | Vertical Jump 3              | Vertical Jump 3   | Vertical Jump 3     |
| 垂直ジャンプ4    | suichokujanpu4        | Vertical Jump 4              | Vertical Jump 4   | Vertical Jump 4     |
| 垂直ジャンプ5    | suichokujanpu5        | Vertical Jump 5              | Vertical Jump 5   | Vertical Jump 5     |
| 垂直ジャンプ6    | suichokujanpu6        | Vertical Jump 6              | Vertical Jump 6   | Vertical Jump 6     |
| 垂直ジャンプ7    | suichokujanpu7        | Vertical Jump 7              | Vertical Jump 7   | Vertical Jump 7     |
| 垂直ジャンプ8    | suichokujanpu8        | Vertical Jump 8              | Vertical Jump 8   | Vertical Jump 8     |
| 手裏剣           | shuriken              | Shuriken                     | Shuriken          | Shuriken            |
| たま             | tama                  | Ball                         | Ball              | Bomb                |
| ナイフ           | naifu                 | Knife                        | Knife             | Knife               |
| 剣               | ken                   | Sword                        | Sword             | Sword               |
| 鎚               | tsuchi                | Hammer                       | Hammer            | Flail               |
| 刀               | katana                | Katana                       | Katana            | Katana              |
| 忍者刀           | ninjatō               | Ninja Sword                  | Ninja Sword       | Ninja Blade         |
| 斧               | ono                   | Axe                          | Axe               | Axe                 |
| 槍               | yari                  | Spear                        | Spear             | Polearm             |
| 棒               | bō                    | Pole                         | Stick             | Pole                |
| 騎士剣           | kishiken              | Knight Sword                 | Knight's Sword    | Knight's Sword      |
| 辞書             | jisho                 | Dictionary                   | Dictionary        | Book                |
| CT               | CT                    | CT                           | CT                | CT                  |
| レベル           | reberu                | Level                        | Level             | Level               |
| Exp              | Exp                   | XP                           | EXP               | EXP                 |
| ハイト           | haito                 | Height                       | Height            | Height              |
| 素数             | sosū                  | Prime Number                 | Prime             | Prime               |
| 5                | 5                     | 5                            | Multiple of 5     | Multiple of 5       |
| 4                | 4                     | 4                            | Multiple of 4     | Multiple of 4       |
| 3                | 3                     | 3                            | Multiple of 3     | Multiple of 3       |
| 天使の詩         | tenshinoshi           | Angelic Verse                | Angel Song        | Seraph Song         |
| 命の詩           | inochinoshi           | Life's Verse                 | Life Song         | Life's Anthem       |
| 応援歌           | ōenka                 | Fight Song                   | Cheer Song        | Rousing Melody      |
| 戦いの詩         | tatakainoshi          | Battle Hymn                  | Battle Song       | Battle Chant        |
| 魔力の詩         | maryokunoshi          | Magical Hymn                 | Magic Song        | Magickal Refrain    |
| 名もなき詩       | namonakishi           | Nameless Verse               | Nameless Song     | Nameless Song       |
| ラストソング     | rasutosongu           | Last Song                    | Last Song         | Finale              |
| ウイッチハント   | wicchihanto           | Witch Hunt                   | Witch Hunt        | Witch Hunt          |
| ウイズナイブス   | wizunaibusu           | Wiznaibus                    | Wiznaibus         | Mincing Minuet      |
| スローダンス     | surōdansu             | Slow Dance                   | Slow Dance        | Slow Dance          |
| ポルカポルカ     | porukaporuka          | Polka Polka                  | Polka Polka       | Polka               |
| アンフェイス     | anfeisu               | Unfaith                      | Disillusion       | Heathen Frolick     |
| ネイムレスダンス | neimuresudansu        | Nameless Dance               | Nameless Dance    | Forbidden Dance     |
| ラストダンス     | rasutodansu           | Last Dance                   | Last Dance        | Last Waltz          |
| エール           | ēru                   | Yell                         | Yell              | Tailwind            |
| おまじない       | amajinai              | Good Luck Charm              | Wish              | Chant               |
| はげます         | hogemasu              | Encourage                    | Cheer Up          | Steel               |
| さけぶ           | sakebu                | Clamor                       | Scream            | Shout               |
| 足を狙う         | ashionerau            | Leg Aim                      | Leg Aim           | Leg Shot            |
| 腕を狙う         | udeonerau             | Arm Aim                      | Arm Aim           | Arm Shot            |
| 邪心封印         | jashinfūin            | Seal Evil                    | Seal Evil         | Seal Evil           |
| 不動無明剣       | fudōmumyōken          | Infinite Ignorance Sword     | Stasis Sword      | Judgment Blade      |
| 乱命割殺打       | midaremeikatsusatsuda | Chaotic Life Render          | Split Punch       | Cleansing Strike    |
| 北斗骨砕打       | hokutohonekudatsu     | Hokuto Bonecrusher           | Crush Punch       | Northswain's Strike |
| 無双稲妻突き     | musōinajumatokki      | Peerless Lightning Thrust    | Lightning Stab    | Hallowed Bolt       |
| 聖光爆裂破       | seikōbakuretsuha      | Holy Explosion               | Holy Explosion    | Divine Ruination    |
| 強甲破点突き     | kyōkōhatenzuki        | Armor-Piercing Thrust        | Shellburst Stab   | Crush Armor         |
| 星天爆撃打       | seitenbakugekida      | Starry Explosive Strike      | Blastar Punch     | Crush Helm          |
| 冥界恐叫打       | meikaikowasakebega    | Hell's Fearsome Strike       | Hellcry Punch     | Crush Weapon        |
| 咬撃氷狼破       | kōgekihyōrōha         | Gnawing Icewolf's Strike     | Icewolf Bite      | Crush Accessory     |
| 暗の剣           | annoken               | Dark Sword                   | Dark Sword        | Duskblade           |
| 闇の剣           | yaminoken             | Endarkened Sword             | Night Sword       | Shadowblade         |
| 天鼓雷音         | tenkoraion            | Heaven's Thunderclap         | Heaven Thunder    | Heaven's Wrath      |
| 金剛七剣         | vajrashichiken        | Adamantine Seven-Edge        | Diamond Sword     | Adamantine Blade    |
| 水磨龍穴         | suimaryūkettsu        | Waterwyrm's Den              | Hydragon Pit      | Maelstrom           |
| 大虚空蔵         | daikokuzō             | Great Akasgarbha             | Space Storage     | Celestial Void      |
| 天魔鬼神         | tenmakishin           | Heaven's Archfiend           | Sky Demon         | Divinity            |
| 裏天鼓雷音       | uratenkoraion         | Hell's Thunderclap           | Heaven Bltback    | Hell's Wrath        |
| 裏阿修羅         | uraashura             | Ashura Inversion             | Asura Back        | Nether Ashura       |
| 裏金剛七剣       | uravajrashichiken     | Adamantine Edge Inversion    | Dia Swd Back      | Nether Blade        |
| 裏水磨龍穴       | urasuimaryūkettsu     | Waterwyrm's Den Inversion    | Dragn Pit Back    | Nether Maelstrom    |
| 裏大虚空蔵       | uradaikokuzō          | Great Akasgarbha Inversion   | Space Str Back    | Corporeal Void      |
| 裏天魔鬼神       | uratenmakishin        | Hell's Archfiend             | Sky Demon Back    | Impiety             |
| アイスブレス     | aisuburesu            | Ice Breath                   | Ice Bracelet      | Ice Breath          |
| ファイアブレス   | faiaburesu            | Fire Breath                  | Fire Bracelet     | Fire Breath         |
| サンダーブレス   | sandāburesu           | Thunder Breath               | Thunder Bracelet  | Thunder Breath      |
| 竜ならし         | ryūnarashi            | Dragonsong                   | Dragon Tame       | Dragon's Charm      |
| 竜介抱           | ryūkaihō              | Dragoncare                   | Dragon Care       | Dragon's Gift       |
| 竜パワーアップ   | ryūpawāappu           | Dragon Power Up              | Dragon PowerUp    | Dragon's Might      |
| 竜レベルアップ   | ryūreberuappu         | Dragon Level Up              | Dragon LevelUp    | Dragon's Speed      |
| ホーリーブレス   | hōrīburesu            | Holy Breath                  | Holy Bracelet     | Holy Breath         |
| ブレイバー       | bureibā               | Braver                       | Braver            | Brave Slash         |
| 凶斬り           | kyōgiri               | Wicked Slash                 | Cross-slash       | Cross Slash         |
| 破晄撃           | hadōgeki              | Wave Strike                  | Blade Beam        | Blade Beam          |
| クライムハザード | kuraimuhazādo         | Climb Hazard                 | Climhazzard       | Climhazzard         |
| メテオレイン     | meteorein             | Meteor Rain                  | Meteorain         | Meteorain           |
| 画龍点睛         | garyōtensei           | Finishing Touch              | Finish Touch      | Finishing Touch     |
| 裏超究武神覇斬   | urachōkyūbushinhazan  | Sublime God's Inverse Strike | Omnislash         | Omnislash           |
| 桜華狂咲         | ōkakyōshō             | Cherry Chaos                 | Cherry Blossom    | Cherry Blossom      |
| 破壊する         | hakaisuru             | Destroy                      | Destroy           | Destroy             |
| 圧縮する         | asshukusuru           | Compress                     | Compress          | Compress            |
| 処理する         | shorisuru             | Dispose                      | Dispose           | Dispose             |
| 粉砕する         | funsaisuru            | Pulverize                    | Crush             | Pulverize           |
| エナジール       | enajīru               | Energy                       | Energy            | Energize            |
| パラサイト       | parasaito             | Parasite                     | Parasite          | Parasite            |
| ショック         | shokku                | Shock                        | Shock             | Vengeance           |
| ディファレンス   | difarensu             | Difference                   | Difference        | Manaburn            |
| 星天停止         | toshitakateishi       | Heavenly Star's Cessation    | Galaxy Stop       | Celestial Stasis    |
| 誘惑             | yūwaku                | Temptation                   | Allure            | Charm               |
| 影縫い           | kagenui               | Shadow Stitch                | Shadow Stitch     | Shadowbind          |
| 封印             | fūin                  | Seal                         | Seal              | Seal                |
| 息根止           | ikinonedome           | Stop Breath                  | Stop Bracelet     | Suffocate           |
| マジックルーイン | majikkurūin           | Ruin Magic                   | Magic Ruin        | Magicksap           |
| スピードルーイン | supīdorūin            | Ruin Speed                   | Speed Ruin        | Speedsap            |
| パワールーイン   | pawārūin              | Ruin Power                   | Power Ruin        | Powersap            |
| マインドルーイン | maindorūin            | Ruin Mind                    | Mind Ruin         | Mindsap             |
| 吸血             | kyūketsu              | Bloodsucker                  | Blood Suck        | Vampire             |
| バイオ           | baio                  | Bio                          | Bio               | Bio                 |
| バイオラ         | baiora                | Biora                        | Bio 2             | Biora               |
| バイオガ         | baioga                | Bioga                        | Bio 3             | Bioga               |
| ライフブレイク   | raifubureiku          | Life Break                   | Life Break        | Karma               |
| ダークホーリー   | dākuhōrī              | Dark Holy                    | Dark Holy         | Unholy Darkness     |
| ギガフレア       | gigafurea             | Gigaflare                    | Giga Flare        | Gigaflare           |
| ナノフレア       | nanofurea             | Nanoflare                    | Nanoflare         | Nanoflare           |
| ハリケーン       | harikēn               | Hurricane                    | Hurricane         | Twister             |
| アルマゲスト     | arumagesuto           | Almagest                     | Ulmaguest         | Almagest            |
| 悪夢             | akumu                 | Nightmare                    | Nightmare         | Nightmare           |
| 鶏走             | torisō                | Bird Race                    | Chicken Race      | Fowlheart           |
| 悪寒             | okan                  | Ague                         | Death Cold        | Ague                |
| 失声             | shitsugoe             | Aphonia                      | Lose Voice        | Aphony              |
| 喪失             | sōshitsu              | Forfeit                      | Loss              | Befuddle            |
| 暗黒             | ankoku                | Darkness                     | Darkness          | Darkness            |
| 呪縛             | jubaku                | Binding                      | Spell             | Bind                |
| グランドクロス   | gurandokurosu         | Grand Cross                  | Grand Cross       | Grand Cross         |
| へびつかい       | hebitsukai            | Summon Snake                 | Snake Carrier     | Snake User          |
| 毒ガエル         | dokugaeru             | Poison Frog                  | Poison Frog       | Poisonous Frog      |
| ミドガルズオルム | midogaruzuorumu       | Midgardsormr                 | Midgar Swarm      | Midgardsormr        |

### Enemy Action Abilities

| Japanese         | Romaji            | Síle                 | PlayStation    | War of the Lions |
| ---------------- | ----------------- | -------------------- | -------------- | ---------------- |
| チョコアタック   | chokoatakku       | Choco Attack         | Choco Attack   | Choco Beak       |
| チョコボール     | chokobōru         | Choco Ball           | Choco Ball     | Choco Pellets    |
| チョコメテオ     | chokometeo        | Choco Meteor         | Choco Meteor   | Choco Meteor     |
| チョコケアル     | chokokearu        | Choco Cure           | Choco Cure     | Choco Cure       |
| チョコエスナ     | chokoesuna        | Choco Null Status    | Choco Esuna    | Choco Esuna      |
| タックル         | takkuru           | Tackle               | Tackle         | Tackle           |
| 回転パンチ       | kaitenpanchi      | Spinning Punch       | Turn Punch     | Spin Punch       |
| ゴブリンパンチ   | goburinpanchi     | Goblin Punch         | Goblin Punch   | Goblin Punch     |
| めつぶし         | metsubushi        | Eyepoke              | Eye Gouge      | Eye Gouge        |
| ミュウチレイト   | myuchireito       | Mutilate             | Mutilate       | Bloodfest        |
| かみつく         | kamitsuku         | Bite                 | Bite           | Bite             |
| コボム           | kobomu            | Li'l Bomb            | Small Bomb     | Bomblet          |
| フレイムアタック | fureimuatakku     | Flame Attack         | Flame Attack   | Flame Attack     |
| スパーク         | supāku            | Spark                | Spark          | Spark            |
| 自爆             | jibaku            | Self-Destruct        | Self Destruct  | Self-Destruct    |
| ひっかく         | hikkaku           | Scratch              | Scratch        | Claw             |
| ネコキック       | nekokikku         | Kitty Kick           | Cat Kick       | Cat Scratch      |
| 毒牙             | dokuga            | Poison Fang          | Poison Nail    | Venom Fang       |
| ブラスター       | burasutā          | Blaster              | Blaster        | Blaster          |
| 吸血             | kyūketsu          | Bloodsuck            | Blood Suck     | Vampire          |
| 触手             | shokushu          | Tentacle             | Tentacle       | Tentacles        |
| すみ             | sumi              | Ink                  | Black Ink      | Ink              |
| 怪音波           | kaionnami         | Mysterious Sound     | Odd Soundwave  | Dischord         |
| マインドブラスト | maindoburasuto    | Mindblast            | Mind Blast     | Mind Blast       |
| レベルブラスト   | reberuburasuto    | Level Blast          | Level Blast    | Level Drain      |
| 手刀             | tegatana          | Chop                 | Knife Hand     | Chop             |
| サンダーソウル   | sandāsōru         | Thunder Soul         | Thunder Soul   | Thunder Anima    |
| アクアソウル     | akuasōru          | Aqua Soul            | Aqua Soul      | Water Anima      |
| アイスソウル     | aisusōru          | Ice Soul             | Ice Soul       | Ice Anima        |
| ウィンドソウル   | windosōru         | Wind Soul            | Wind Soul      | Wind Anima       |
| 霊気を飛ばす     | reikitobasu       | Flying Spirit        | Throw Spirit   | Ectoplasm        |
| スリープタッチ   | surīputacchi      | Sleep Touch          | Sleep Touch    | Sleep Touch      |
| グリースタッチ   | gurīsutacchi      | Grease Touch         | Grease Touch   | Oily Touch       |
| ドレインタッチ   | doreintacchi      | Drain Touch          | Drain Touch    | Drain Touch      |
| ゾンビタッチ     | zonbitacchi       | Zombie Touch         | Zombie Touch   | Zombie Touch     |
| 翼撃             | tsubasageki       | Wing Attack          | Wing Attack    | Wing Buffet      |
| あくまの視線     | akumanoshisen     | Devil's Gaze         | Look of Devil  | Bewitching Gaze  |
| 恐怖の視線       | kyōfunoshisen     | Frightful Gaze       | Look of Fright | Dread Gaze       |
| 死の宣告         | shinosenkoku      | Death Sentence       | Death Sentence | Doom             |
| サークル         | sākuru            | Circle               | Circle         | Beam             |
| かきむしる       | kakimushiru       | Pluck                | Scratch Up     | Talon Dive       |
| フェザーボム     | fezābomu          | Feather Bomb         | Feather Bomb   | Featherbomb      |
| 光ものが好き     | hikarimonogasuki  | Magpie's Love        | Shine Lover    | Glitterlust      |
| くちばし         | kuchibashi        | Beak                 | Beak           | Beak             |
| ピーキング       | pīkingu           | Peck                 | Beaking        | Peck             |
| ちょとつもうしん | chototsumōshin    | Rush                 | Straight Dash  | Reckless Charge  |
| はないき         | hanaiki           | Snort                | Nose Bracelt   | Snort            |
| ぷ～っ           | pu~u              | Squeak               | Pooh-          | Toot             |
| ブヒー           | buhī              | Oink                 | Oink           | Squeak           |
| 食べてもらう     | tabetemorau       | Eat!                 | Please Eat     | Bequeath Bacon   |
| 木の葉乱舞       | kinoharanbu       | Dance of Tree-Leaves | Leaf Dance     | Leaf Rain        |
| まもりの精       | mamorinosei       | Dryad's Protection   | Protect Spirit | Guardian Nymph   |
| 貝の精           | kainosei          | Dryad's Shell        | Spirit of Life | Life Nymph       |
| 命の精           | inochinosei       | Dryad's Life         | Clam Spirit    | Spirit Nymph     |
| 魔法の精         | mahōsei           | Dryad's Magic        | Magic Spirit   | Magick Nymph     |
| ふりおろす       | furiorosu         | Bring Down           | Shake Off      | Pickaxe          |
| ふりまわす       | furimawasu        | Brandish             | Wave Around    | Feral Spin       |
| 火を吹く         | hiofuku           | Burst Into Flame     | Blow Fire      | Breath Fire      |
| 力をためる       | chikaraotameru    | Accumulate Energy    | Gather Power   | Beef Up          |
| タイタンのまね   | taitannomane      | Imitate Titan        | Mimic Titan    | Earthsplitter    |
| なめる           | nameru            | Lick                 | Lick           | Lick             |
| ねとねと液       | netonetoeki       | Goopy Liquid         | Goo            | Goo              |
| 臭い息           | kusaiiki          | Bad Breath           | Bad Bracelet   | Bad Breath       |
| モルボル菌       | moruborukin       | Mortboule Fungus     | Moldball Virus | Malboro Spores   |
| 突きあげる       | tsukiageru        | Push Up              | Stab Up        | Gore             |
| しゃくりあげる   | shakuriageru      | Sob                  | Sudden Cry     | Heave            |
| 尻尾振り回し     | shippofurimawashi | Brandish Tail        | Tail Swing     | Tail Sweep       |
| トリプルアタック | toripuruatakku    | Triple Attack        | Triple Attack  | Tri-Attack       |
| トリプルブレス   | toripuruburesu    | Triple Breath        | Triple Brcelet | Tri-Breath       |
| トリプルフレイム | toripurufureimu   | Triple Flame         | Triple Flame   | Tri-Flame        |
| トリプルサンダー | toripurusandā     | Triple Thunder       | Triple Thunder | Tri-Thunder      |
| 暗黒の囁き       | ankokunosasayaki  | Whispering Darkness  | Dark Whisper   | Dark Whisper     |

### Reactions

| Japanese         | Romaji           | Síle                  | PlayStation    | War of the Lions     |
| ---------------- | ---------------- | --------------------- | -------------- | -------------------- |
| 反撃タックル     | hangekitakkuru   | Counter-Tackle        | Counter Tackle | Counter Tackle       |
| カウンター       | kauntā           | Counter               | Counter        | Counter              |
| 風水返し         | fūsuikaeshi      | Return Geomancy       | Counter Flood  | Nature's Wrath       |
| 魔法返し         | mahōkaeshi       | Return Magic          | Counter Magic  | Magick Counter       |
| 肉斬骨断         | nikukihonedan    | Sever Flesh from Bone | Meatbone Slash | Bonecrusher          |
| ハメドる         | hamedoru         | Counter First         | Hamedo         | First Strike         |
| ダメージ分配     | damējibunpai     | Split Damage          | Damage Split   | Soulbind             |
| 見切る           | mikiru           | Abandon               | Abandon        | Reflexes             |
| 装備武器ガード   | sōbibukigādo     | Protect Equipment     | Weapon Guard   | Parry                |
| ＭＰすり替え     | MPsurikae        | Swap MP               | MP Switch      | Manashield           |
| 矢かわし         | yakawashi        | Block Arrows          | Arrow Guard    | Archer's Bane        |
| 白刃取り         | shirahadori      | Shirahadori           | Blade Grasp    | Shirahadori          |
| 潜伏             | senpuku          | Conceal               | Sunken State   | Vanish               |
| 警戒             | keikai           | Vigilance             | Caution        | Vigilance            |
| 耳指ガード       | mimiyubigādo     | Block Diplomacy       | Finger Guard   | Earplug              |
| キャッチ         | kyacchi          | Catch                 | Catch          | Sticky Fingers       |
| オートポーション | ōtopōshon        | Auto-Potion           | Auto Potion    | Auto-Potion          |
| リジェネーター   | rejenētā         | Regenerator           | Regenerator    | Regenerate           |
| 竜の魂           | ryūnotamashī     | Dragon's Spirit       | Dragon Spirit  | Dragon Heart         |
| 瀕死HP回復       | hinshiHPkaifuku  | SOS HP Recovery       | HP Restore     | Critical: Recover HP |
| 瀕死MP回復       | hinshiMPkaifuku  | SOS MP Recovery       | MP Restore     | Critical: Recover MP |
| 瀕死クイック     | hinshikuikku     | SOS Quick             | Critical Quick | Critical: Quick      |
| 使用MP吸収       | shiyōMPkyūshū    | Absorb MP             | Absorb Used MP | Absorb MP            |
| あまり振り分け   | omarifuriwake    | Divide Remainder      | Distribute     | Cup of Life          |
| ギルガメの心     | girugamenokokoro | Gil Turtle's Heart    | Gilgame Heart  | Gil Snapper          |
| Speedセーブ      | Speedsēbu        | Save Speed            | Speed Save     | Adrenaline Rush      |
| Aセーブ          | Asēbu            | Save Attack           | A Save         | Fury                 |
| MAセーブ         | MAsēbu           | Save MAttack          | MA Save        | Magick Boost         |
| フェイスアップ   | feisuappu        | Faith Up              | Face Up        | Faith Boost          |
| ブレイブアップ   | bureibuappu      | Brave Up              | Brave Up       | Bravery Boost        |

### Support Abilities

| Japanese         | Romaji            | Síle              | PlayStation    | War of the Lions  |
| ---------------- | ----------------- | ----------------- | -------------- | ----------------- |
| 魔法攻撃力UP     | mahōkōgekiryokuUP | Magic Attack Up   | Magic Att. UP  | Arcane Strength   |
| 魔法防御力UP     | mahōbōgyoryokuUP  | Magic Defense Up  | Magic Def. UP  | Arcane Defense    |
| 攻撃力UP         | kōgekiryokuUP     | Attack Up         | Attack UP      | Attack Boost      |
| 防御力UP         | bōgyoryokuUP      | Defense Up        | Defense UP     | Defense Boost     |
| 取得JpUP         | shutokuJpUP       | Gained JP Up      | Gained JP-UP   | JP Boost          |
| 取得ExpUP        | shutokuExpUP      | Gained XP Up      | Gained EXP-UP  | EXP Boost         |
| ショートチャージ | shōtochāji        | Short Charge      | Short Charge   | Swiftness         |
| 消費MP半減       | shōhiMPhangen     | Half MP           | Half of MP     | Halve MP          |
| メンテナンス     | mentanansu        | Maintenance       | Maintenance    | Maintenance       |
| アイテム投げ     | aitemunage        | Throw Item        | Throw Item     | Throw Items       |
| 格闘             | kakutō            | Brawling          | Martial Arts   | Brawler           |
| 調教             | chōkyō            | Animal Training   | Train          | Tame              |
| まじゅう使い     | majūtsukai        | Beast Tamer       | Monster Skill  | Beastmaster       |
| まじゅう語       | majūko            | Beast Talk        | Monster Talk   | Beast Tongue      |
| 二刀流           | nitōryū           | Dual Wield        | Two Swords     | Dual Wield        |
| 両手持ち         | ryōtemoki         | Two-Handed        | Two Hands      | Doublehand        |
| 精神統一         | seishintōitsu     | Concentration     | Concentrate    | Concentration     |
| 密猟             | mitsuryō          | Poach             | Secret Hunt    | Poach             |
| 防御             | bōgyo             | Defense           | Defend         | Defend            |
| 装備変更         | sōbihenkō         | Change Equipment  | Equip Change   | Reequip           |
| 重装備可能       | jūsōbikanō        | Equip Heavy Armor | Equip Armor    | Equip Heavy Armor |
| 盾装備可能       | tatesōbikanō      | Equip Shields     | Equip Shield   | Equip Shields     |
| 剣装備可能       | kensōbikanō       | Equip Swords      | Equip Sword    | Equip Swords      |
| 自動弓装備可能   | jidōyumisōbikanō  | Equip Crossbows   | Equip Crossbow | Equip Crossbows   |
| 斧装備可能       | onosōbikanō       | Equip Axes        | Equip Axe      | Equip Axes        |
| 銃装備可能       | jūsōbikanō        | Equip Guns        | Equip Gun      | Equip Guns        |
| 槍装備可能       | yarisōbikanō      | Equip Spears      | Equip Spear    | Equip Polearms    |
| 刀装備可能       | katanasōbikanō    | Equip Katanas     | Equip Katana   | Equip Katanas     |

### Map Abilities

| Japanese         | Romaji              | Síle                   | PlayStation | War of the Lions |
| ---------------- | ------------------- | ---------------------- | ----------- | ---------------- |
| 高低差無視       | kōteisamushi        | Ignore Elevation       | Ignore Height  | Ignore Elevation |
| 飛行移動         | hikōidō             | Flight                 | Fly            | Fly              |
| テレポ           | terepo              | Teleportation          | Teleport       | Teleport         |
| 水面移動         | suimenidō           | Swimming               | Move in Water  | Swim             |
| 水上移動         | suijōidō            | Waterwalking           | Walk on Water  | Waterwalking     |
| 水中移動         | suichūidō           | Waterbreathing         | Move Undrwater | Waterbreathing   |
| 溶岩上移動       | yōganjōidō          | Lavawalking            | Move on Lava   | Lava Walking     |
| 移動距離地形無視 | idōkyorichikeimushi | Ignore Terrain Effects | Any Ground     | Ignore Terrain   |
| 移動距離天候無視 | idōkyoritenkōmushi  | Ignore Weather Effects | Any Weather    | Ignore Weather   |
| 浮遊移動         | fuyūidō             | Floating               | Float          | Levitate         |
| アイテム発見移動 | aitemuhakkenidō     | Find Treasure          | Move-Find Item | Treasure Hunter  |
| HP回復移動       | HPkaifukuidō        | HP Recovery            | Move HP Up     | Lifefont         |
| MP回復移動       | MPkaifukuidō        | MP Recovery            | Move MP Up     | Manafont         |
| Exp獲得移動      | Expkakutokuidō      | Gain XP                | Move-Get Exp   | Accrue EXP       |
| Jp獲得移動       | Jpkautokuidō        | Gain JP                | Move-Get Jp    | Accrue JP        |

### Magic

| Japanese   | Romaji   | Síle        | PlayStation   | War of the Lions   |
| ---------- | -------- | ----------- | ------------- | ------------------ |
| アスピル   | asupiru  | Aspirate    | Aspel         | Syphon             |
| アルテマ   | arutema  | Ultima      | Ultima        | Ultima             |
| アレイズ   | areizu   | Raisega     | Raise 2       | Arise              |
| イノセン   | inosen   | Innocent    | Innocent      | Doubt              |
| ウォール   | wōru     | Wall        | Wall          | Wall               |
| エスナ     | esuna    | Null Status | Esuna         | Esuna              |
| クイック   | kuikku   | Quick       | Quick         | Quick              |
| クエイク   | kueiku   | Quake       | Quake         | Quake              |
| グラビテ   | gurabite | Gravity     | Demi          | Gravity            |
| グラビジャ | gurabija | Gravityja   | Gravi 2       | Gravija            |
| グラビガ   | gurabiga | Gravityga   | Demi 2        | Graviga            |
| ケアル     | kearu    | Cure        | Cure          | Cure               |
| ケアルラ   | kearura  | Curera      | Cure 2        | Cura               |
| ケアルガ   | kearuga  | Curega      | Cure 3        | Curaga             |
| ケアルジャ | kearuja  | Cureja      | Cure 4        | Curaja             |
| コンフュ   | konfyu   | Confuse     | Confuse       | Confuse            |
| コンフジャ | konfuja  | Confusega   | Confuse 2     | Confuseja          |
| サイレス   | sairesu  | Silence     | Silence       | Silence            |
| サンダー   | sandā    | Thunder     | Bolt          | Thunder            |
| サンダラ   | sandara  | Thundera    | Bolt 2        | Thundara           |
| サンダガ   | sandaga  | Thunderga   | Bolt 3        | Thundaga           |
| サンダジャ | sandaja  | Thunderja   | Bolt 4        | Thundaja           |
| シェル     | sheru    | Shell       | Shell         | Shell              |
| シェルジャ | sheruja  | Shellga     | Shell 2       | Shellja            |
| ショック！ | shokku!  | Shock!      | Shock!        | Vengeance          |
| ストップ   | sutoppu  | Stop        | Stop          | Stop               |
| スリプル   | suripuru | Sleep       | Sleep         | Sleep              |
| スリプジャ | suripuja | Sleepga     | Sleep 2       | Sleepja            |
| スロウ     | surō     | Slow        | Slow          | Slow               |
| スロウジャ | surōja   | Slowga      | Slow 2        | Slowja             |
| ゾンビー   | zonbī    | Zombie      | Zombie        | Zombie             |
| チキン     | chikin   | Chicken     | Chicken       | Chicken            |
| デス       | desu     | Death       | Death         | Death              |
| デスペル   | desuperu | Dispel      | Despair       | Dispel             |
| デスペジャ | desupeja | Dispelra    | Despair 2     | Dispelja           |
| デスペナ   | desupena | Dispelga    | Deathspell 2  | Dispelna           |
| トルネド   | torunedo | Tornado     | Tornado       | Tornado            |
| トード     | tōdo     | Toad        | Toad          | Toad               |
| トードジャ | tōdoja   | Toadga      | Toad 2        | Toadja             |
| ドレイン   | dorein   | Drain       | Drain         | Drain              |
| ドンアク   | donaku   | Disable     | Don't Act     | Disable            |
| ドンムブ   | donmubu  | Immobilize  | Don't Move    | Immobilize         |
| バーサク   | bāsaku   | Berserk     | Berserk       | Berserk            |
| ファイア   | faia     | Fire        | Fire          | Fire               |
| ファイラ   | faira    | Firera      | Fire 2        | Fira               |
| ファイガ   | faiga    | Firega      | Fire 3        | Firaga             |
| ファイジャ | faija    | Fireja      | Fire 4        | Firaja             |
| フェイス   | feisu    | Faith       | Faith         | Faith              |
| フレア     | furea    | Flare       | Flare         | Flare              |
| フレアジャ | fureaja  | Flarega     | Flare 2       | Flareja            |
| ブライン   | burain   | Blind       | Blind         | Blind              |
| ブライジャ | buraija  | Blindga     | Blind 2       | Blindja            |
| ブリザド   | burizado | Blizzard    | Ice           | Blizzard           |
| ブリザラ   | burizara | Blizzardra  | Ice 2         | Blizzara           |
| ブリザガ   | burizaga | Blizzardga  | Ice 3         | Blizzaga           |
| ブリザジャ | burizaja | Blizzardja  | Ice 4         | Blizzaja           |
| ブレイク   | bureiku  | Break       | Break         | Break              |
| プロテス   | purotesu | Protect     | Protect       | Protect            |
| プロテジャ | puroteja | Protectga   | Protect 2     | Protectja          |
| ヘイスト   | heisuto  | Haste       | Haste         | Haste              |
| ヘイスジャ | heisuja  | Hastega     | Haste 2       | Hasteja            |
| ホーリー   | hōrī     | Holy        | Holy          | Holy               |
| ポイズン   | poizun   | Poison      | Poison        | Poison             |
| マバリア   | mabaria  | M. Barrier  | Mbarrier      | Aegis              |
| ミュート   | myūto    | Mute        | Mute          | Disempower         |
| メテオ     | meteo    | Meteor      | Meteor        | Meteor             |
| メルトン   | meruton  | Meltdown    | Melt          | Meltdown           |
| リジエネ   | rijene   | Regen       | Regen         | Regen              |
| リタンジャ | ritanja  | Returnga    | Return 2      | Return             |
| リフレク   | rifureku | Reflect     | Reflect       | Reflect            |
| リレイズ   | rireizu  | Reraise     | Reraise       | Reraise            |
| レイズ     | reizu    | Raise       | Raise         | Raise              |
| レビテト   | rebitedo | Levitate    | Float         | Float              |

### Summons

| Japanese       | Romaji     | Síle        | PlayStation | War of the Lions   |
| -------------- | ---------- | ----------- | ----------- | ------------------ |
| モーグリ       | mōguri     | Mógli       | Moogle      | Moogle             |
| シヴァ         | shiva      | Shiva       | Shiva       | Shiva              |
| ラムウ         | ramū       | Ramuh       | Ramuh       | Ramuh              |
| イフリート     | ifurīto    | Ifrit       | Ifrit       | Ifrit              |
| タイタン       | taitan     | Titan       | Titan       | Titan              |
| ゴーレム       | gōremu     | Golem       | Golem       | Golem              |
| カーバンクル   | kābankuru  | Carbuncle   | Carbunkle   | Carbuncle          |
| バハムート     | bahamūto   | Bahamut     | Bahamut     | Bahamut            |
| オーディン     | ōdin       | Odin        | Odin        | Odin               |
| リヴァイアサン | rivaiasan  | Leviathan   | Leviathan   | Leviathan          |
| サラマンダー   | saramandā  | Salamander  | Salamander  | Salamander         |
| シルフ         | shirufu    | Sylph       | Silf        | Sylph              |
| フェアリー     | fearī      | Faerie      | Fairy       | Fairy              |
| リッチ         | ricchi     | Lich        | Rich        | Lich               |
| クリュプス     | kuryupusu  | Cyclops     | Cyclops     | Cyclops            |
| ゾディアーク   | zodiāku    | Zodiarc     | Zodiac      | Zodiark            |

## Items

### Consumables

| Japanese         | Romaji           | Síle          | PlayStation    | War of the Lions   |
| ---------------- | ---------------- | ------------- | -------------- | ------------------ |
| ポーション       | pōshon           | Potion        | Potion         | Potion             |
| ハイポーション   | haipōshon        | High Potion   | Hi-Potion      | Hi-Potion          |
| エクスポーション | ekusupōshon      | Ex Potion     | X-Potion       | X-Potion           |
| エーテル         | ēteru            | Ether         | Ether          | Ether              |
| ハイエーテル     | haiēteru         | High Ether    | Hi-Ether       | Hi-Ether           |
| エリクサー       | erikusā          | Elixir        | Elixir         | Elixir             |
| 毒消し           | dokukeshi        | Antidote      | Antidote       | Antidote           |
| 目薬             | megusuri         | Eye Drops     | Eye Drop       | Eye Drops          |
| やまびこ草       | yamabikosō       | Echo Grass    | Echo Grass     | Echo Herbs         |
| 乙女のキッス     | otomenokissu     | Maiden's Kiss | Maiden's Kiss  | Maiden's Kiss      |
| 金の針           | kinnohari        | Gold Needle   | Soft           | Gold Needle        |
| 聖水             | seisui           | Holy Water    | Holy Water     | Holy Water         |
| 万能薬           | bannōyaku        | Panacea       | Remedy         | Remedy             |
| フェニックスの尾 | fenikkusunoo     | Phoenix Tail  | Phoenix Down   | Phoenix Down       |
| かとんのたま     | katonnotama      | Fire Bomb     |	Fire Ball      | Flameburst Bomb    |
| ひょうすいのたま | hyōsuinotama     | Water Bomb    | Water Ball     | Snowmelt Bomb      |
| らいじんのたま   | raidennotama     | Thunder Bomb  |	Lightning Ball | Spark Bomb         |
| 手裏剣           | shuriken         | Shuriken      | Shuriken       | Shuriken           |
| 風魔の手裏剣     | fūmanoshuriken   | Fuma Shuriken |	Magic Shuriken | Fuma Shuriken      |
| 柳生の漆黒       | yagyūnoshikkoku  | Yagyu's Black | Yagyu Darkness | Yagyu Darkrood     |

### Weapons

| Japanese         |_ Romaji               | Síle                | PlayStation     | War of the Lions    |
| ---------------- | --------------------- | ------------------- | --------------- | ------------------- |
| ナグラロク       | naguraroku            | Nagrarok            | Nagrarock       | Nagrarok            |
| ブロードソード   | burōdosōdo            | Broadsword          | Broad Sword     | Broadsword          |
| ロングソード     | rongusōdo             | Longsword           | Long Sword      | Longsword           |
| アイアンソード   | aiansōdo              | Iron Sword          | Iron Sword      | Iron Sword          |
| ミスリルソード   | misurirusōdo          | Mithril Sword       | Mythril Sword   | Mythril Sword       |
| ブラッドソード   | buraddosōdo           | Blood Sword         | Blood Sword     | Blood Sword         |
| さんごの剣       | sangonoken            | Coral Blade         | Coral Sword     | Coral Sword         |
| 古代の剣         | kodainoken            | Ancient Blade       | Ancient Sword   | Ancient Sword       |
| 眠りの剣         | nemurinotsurugi       | Sleep Saber         | Sleep Sword     | Sleep Blade         |
| ダイアソード     | daiasōdo              | Diamond Sword       | Diamond Sword   | Diamond Sword       |
| マテリアブレイド | materiabureido        | Materia Blade       | Materia Blade   | Materia Blade       |
| プラチナソード   | purachinasōdo         | Platinum Sword      | Platinum Sword  | Platinum Sword      |
| アイスブランド   | aisuburando           | Icebrand            | Ice Brand       | Icebrand            |
| ルーンブレイド   | rūnbureido            | Rune Blade          | Rune Blade      | Runeblade           |
| ディフェンダー   | difendā               | Defender            | Defender        | Defender            |
| セイブザクイーン | saibuzakuīn           | Save the Queen      | Save the Queen  | Save the Queen      |
| エクスカリバー   | ekusukaribā           | Excalibur           | Excalibur       | Excalibur           |
| ラグナロク       | ragunaroku            | Ragnarok            | Ragnarok        | Ragnarok            |
| カオスブレイド   | kaosubureido          | Chaos Blade         | Chaos Blade     | Chaos Blade         |
| 阿修羅           | ashura                | Ashura              | Asura Knife     | Ashura              |
| 虎鉄             | kotetsu               | Kotetsu             | Kotetsu         | Kotetsu             |
| 備前長船         | bizenosafune          | Bizen Osafune       | Bizen Boat      | Osafune             |
| 村雨             | murasame              | Murasame            | Murasame        | Murasame            |
| 天のむら雲       | amenomurakumo         | Ame no Murakumo     | Heaven's Cloud  | Ama-no-Murakumo     |
| 清盛             | kiyomori              | Kiyomori            | Kiyomori        | Kiyomori            |
| 村正             | muramasa              | Muramasa            | Muramasa        | Muramasa            |
| 菊一文字         | kikuichimonji         | Kiku-Ichimonji      | Kikuichimonji   | Kiku-Ichimonji      |
| 正宗             | masamune              | Masamune            | Masamune        | Masamune            |
| 塵地螺鈿飾剣     | chirijiradenkazariken | Ceremonial Katana   | Chirijiraden    | Chirijiraden        |
| ムーンフェイス   | mūnfeisu              | Moonface            | (not present)   | Moonblade           |
| オニオンソード   | onionsōdo             | Onion Sword         | (not present)   | Onion Sword         |
| デュランダル     | durandaru             | Durandal            | (not present)   | Durandal            |
| カオスブリンガー | kaosuburingā          | Chaosbringer        | (not present)   | Chaosbringer        |
| デスブリンガー   | desuburingā           | Deathbringer        | (not present)   | Deathbringer        |
| アロンダイト     | arondaito             | Arondite            | (not present)   | Arondight           |
| バルムンク       | barumungu             | Balmung             | (not present)   | Balmung             |
| ヴァルハラ       | varuhara              | Valhalla            | (not present)   | Valhalla            |
| ダガー           | dagā                  | Dagger              | Dagger          | Dagger              |
| ミスリルナイフ   | misurirunaifu         | Mithril Knife       | Mythril Knife   | Mythril Knife       |
| ブラインナイフ   | burainnaifu           | Blind Knife         | Blind Knife     | Blind Knife         |
| メイジマッシャー | meijimasshā           | Mage Masher         | Mage Masher     | Mage Masher         |
| プラチナメッサー | purachinamessā        | Platinum Messer     | Platina Dagger  | Platinum Dagger     |
| マインゴーシュ   | maingōshu             | Main Gauche         | Main Gauche     | Main Gauche         |
| オリハルコン     | oriharukon            | Orichalcum          | Orichalcum      | Orichalcum Dirk     |
| アサシンダガー   | asashindagā           | Assassin Dagger     | Assassin Dagger | Assassin's Dagger   |
| エアナイフ       | eanaifu               | Air Knife           | Air Knife       | Air Knife           |
| ゾーリンシェイプ | zōrinsheipu           | Zorlin Shape        | Zorlin Shape    | Zwill Straightblade |
| ロングボウ       | rongubō               | Longbow             | Long Bow        | Longbow             |
| 銀の弓           | ginnoyumi             | Silver Bow          | Silver Bow      | Silver Bow          |
| 氷の弓           | kōrinoyumi            | Frost Bow           | Ice Bow         | Ice Bow             |
| ライトニングボウ | raitoningubō          | Lightning Bow       | Lightning Bow   | Lightning Bow       |
| ミスリルの弓     | misurirunoyumi        | Mithril Bow         | Mythril Bow     | Mythril Bow         |
| 風切りの弓       | kazekirinoyumi        | Gale Bow            | Windslash Bow   | Windslash Bow       |
| アルテミスの弓   | arutemisunoyumi       | Artemis's Bow       | Ultimus Bow     | Artemis Bow         |
| 与一の弓         | yoichinoyumi          | Yoichi's Bow        | Yoichi Bow      | Yoichi Bow          |
| ペルセウスの弓   | peruseusunoyumi       | Perseus's Bow       | Perseus Bow     | Perseus Bow         |
| 宿命のサジタリア | shukumeinosajitaria   | Fate of Sagittarius | (not present)   | Sagittarius Bow     |
| ボウガン         | bōgan                 | Bowgun              | Bow Gun         | Bowgun              |
| ナイトキラー     | naitokirā             | Knight Killer       | Night Killer    | Knightslayer        |
| クロスボウ       | kurosubō              | Crossbow            | Cross Bow       | Crossbow            |
| ポイズンボウ     | poizunbō              | Poison Bow          | Poison Bow      | Poison Bow          |
| ハンティングボウ | hantingubō            | Hunting Bow         | Hunting Bow     | Hunting Bow         |
| ガストラフェテス | gasutorafetesu        | Gastraphetes        | Gastrafitis     | Gastrophetes        |
| バトルアックス   | batoruakkusu          | Battle Axe          | Battle Axe      | Battle Axe          |
| 巨人の斧         | kyojinnoono           | Giant's Axe         | Giant Axe       | Giant Axe           |
| スラッシャー     | surasshā              | Slasher             | Slasher         | Slasher             |
| フランシスカ     | furansisuka           | Francisca           | (not present)   | Francisca           |
| ゴールドアックス | gōrudoakkusu          | Gold Axe            | (not present)   | Golden Axe          |
| フレイル         | fureiru               | Flail               | Flail           | Iron Flail          |
| フレイムウィップ | fureimuwippu          | Flame Whip          | Flame Whip      | Flame Mace          |
| モーニングスター | mōningusutā           | Morning Star        | Morning Star    | Morning Star        |
| さそりのしっぽ   | sasorinoshippo        | Scorpion's Tail     | Scorpion Tail   | Scorpion Tail       |
| ウエスペル       | vesuperu              | Vesper              | (not present)   | Vesper              |
| ジャベリン       | jaberin               | Javelin             | Javelin         | Javelin             |
| スピア           | supia                 | Spear               | Spear           | Spear               |
| ミスリルスピア   | misurirusupia         | Mithril Spear       | Mythril Spear   | Mythril Spear       |
| パルチザン       | paruchizan            | Partisan            | Partisan        | Partisan            |
| オベリスク       | oberisuku             | Obelisk             | Oberisk         | Obelisk             |
| ホーリーランス   | hōrīransu             | Holy Lance          | Holy Lance      | Holy Lance          |
| 竜の髭           | ryūnohige             | Dragon's Whisker    | Dragon Whisker  | Dragon Whisker      |
| ゲイボルグ       | geiborugu             | Gae Bolg            | (not present)   | Gae Bolg            |
| グングニル       | gunguniru             | Gungnir             | (not present)   | Gungnir             |
| ジャベリン       | jaberin               | Javelin             | Javelin         | Javelin "II"        |
| 忍び刀           | shinobikatana         | Shinobi Katana      | Hidden Knife    | Ninja Blade         |
| くない           | kunai                 | Kunai               | Ninja Knife     | Kunai               |
| 小太刀           | kodachi               | Kodachi             | Short Edge      | Kodachi             |
| 忍者ロング       | ninjarongu            | Ninja Longedge      | Ninja Edge      | Ninja Longblade     |
| 呪縛刀           | jubakukatana          | Cursed Katana       | Spell Edge      | Spellbinder         |
| 佐助の刀         | sasukenokatana        | Sasuke's Katana     | Sasuke Knife    | Sasuke's Blade      |
| 伊賀忍刀         | iganingatana          | Iga Katana          | Iga Knife       | Iga Blade           |
| 甲賀忍刀         | koganingatana         | Koga Katana         | Koga Knife      | Koga Blade          |
| おろち           | orochi                | Orochi              | (not present)   | Orochi              |
| 羅月伝武         | ragetsudenbu          | Ragetsu-Denbu       | (not present)   | Moonsilk Blade      |
| ペルシア         | perushia              | Persia              | Persia          | Damask Cloth        |
| カシミール       | kashimīru             | Cashmere            | Cashmere        | Cashmere            |
| 両残絹           | ryōzanken             | Ryozan Silk         | Ryozan Silk     | Wyrmweave Silk      |
| オークスタッフ   | ōkusutaffu            | Oak Staff           | Oak Staff       | Oak Staff           |
| ホワイトスタッフ | howaitosutaffu        | White Staff         | White Staff     | White Staff         |
| いやしの杖       | iyashinotsue          | Staff of Solace     | Healing Staff   | Healing Staff       |
| 魔術師の杖       | majutsushinotsue      | Staff of Mages      | Wizard Staff    | Mage's Staff        |
| 蛇の杖           | hebinotsue            | Staff of Snakes     | Rainbow Staff   | Serpent Staff       |
| メイスオブゼウス | meisuobuzeusu         | Zeus's Mace         | Mace of Zeus    | Zeus Mace           |
| ゴールドスタッフ | gōrudosutaffu         | Gold Staff          | Gold Staff      | Golden Staff        |
| 賢者の杖         | kenjanotsue           | Staff of Sages      | Sage Staff      | Staff of the Magi   |
| ニルヴァーナ     | niruvāna              | Nirvana             | (not present)   | Nirvana             |
| ドリームエイド   | dorīmueido            | Dream Aid           | (not present)   | Dreamwaker          |
| ロッド           | roddo                 | Rod                 | Rod             | Rod                 |
| サンダーロッド   | sandāroddo            | Thunder Rod         | Thunder Rod     | Thunder Rod         |
| フレイムロッド   | fureimuroddo          | Flame Rod           | Flame Rod       | Flame Rod           |
| アイスロッド     | aisuroddo             | Ice Rod             | Ice Rod         | Ice Rod             |
| ポイズンロッド   | poizunroddo           | Poison Rod          | Poison Rod      | Poison Rod          |
| ウィザードロッド | wizādoroddo           | Wizard Rod          | Wizard Rod      | Wizard's Rod        |
| ドラゴンロッド   | doragonroddo          | Dragon Rod          | Dragon Rod      | Dragon Rod          |
| フェイスロッド   | feisuroddo            | Faith Rod           | Faith Rod       | Rod of Faith        |
| 星くずのロッド   | hoshikuzunoroddo      | Stardust Rod        | (not present)   | Stardust Rod        |
| プリンセスガード | purinzesugādo         | Princess Guard      | (not present)   | Crown Sceptre       |
| サイプレスパイル | saipuresupairu        | Cypress Spire       | Cypress Rod     | Cypress Pole        |
| バトルバンブー   | batorubanbū           | Battle Bamboo       | Battle Bamboo   | Battle Bamboo       |
| じゃこうしゃく   | jakōshaku             | Musk Shaku          | Musk Rod        | Musk Pole           |
| 鉄扇             | tessen                | Iron Fan            | Iron Fan        | Iron Fan            |
| ゴクウの棒       | gokūnobō              | Goku's Bo           | Gokuu Rod       | Gokuu Pole          |
| 象牙の棒         | zōgenobō              | Ivory Bo            | Ivory Rod       | Ivory Pole          |
| 八角棒           | hakkakubō             | Octagon Bo          | Octagon Rod     | Eight-Fluted Pole   |
| 鯨の髭           | kujiranohige          | Whale's Whisker     | Whale Whisker   | Whale Whisker       |
| ロマンダ銃       | romandajū             | Romanda Gun         | Romanda Gun     | Romanda Gun         |
| ミスリル銃       | misurirujū            | Mithril Gun         | Mythril Gun     | Mythril Gun         |
| 石化銃           | ishikajū              | Stone Gun           | Stone Gun       | Stoneshooter        |
| ブレイズガン     | bureizugan            | Blaze Gun           | Blaze Gun       | Blaze Gun           |
| グレイシャルガン | gureisharugan         | Glacier Gun         | Glacier Gun     | Glacial Gun         |
| ブラストガン     | burasutogan           | Blast Gun           | Blast Gun       | Blaster             |
| ラス・アルゲティ | rasūarugeti           | Ras Algethi         | (not present)   | Ras Algethi         |
| フォーマルハウト | fōmaruhauto           | Fomalhaut           | (not present)   | Fomalhaut           |
| Ｃのバッグ       | Cnobaggu              | C Bag               | C Bag           | Croakadile Bag      |
| Ｐのバッグ       | Pnobaggu              | P Bag               | P Bag           | Pantherskin Bag     |
| Ｈのバッグ       | Hnobaggu              | H Bag               | H Bag           | Hydrascale Bag      |
| ＦＳのバッグ     | FSnobaggu             | FS Bag              | FS Bag          | Fallingstar Bag     |
| バトルディクト   | batorudikuto          | Battle Dictionary   | Battle Dict     | Battle Folio        |
| 怪物辞典         | kaibutsujiten         | Bestiary            | Monster Dict    | Bestiary            |
| パピルスプレイト | papirusupureito       | Papyrus Plate       | Papyrus Plate   | Papyrus Codex       |
| マダレムジエン   | madaremujien          | Madalemgen          | Madlemgen       | Onilex              |
| ラミアの竪琴     | ramianotategoto       | Lamia's Harp        | Ramia Harp      | Lamia's Harp        |
| 血の十二弦       | chinojunigen          | Bloodsoaked Harp    | Bloody Strings  | Bloodstring Harp    |
| 妖精のハープ     | yōseinohāpu           | Faerie Harp         | Fairy Harp      | Fairie Harp         |

### Headgear

| Japanese         |_ Romaji          | Síle            | PlayStation    | War of the Lions   |
| ---------------- | ---------------- | --------------- | -------------- | ------------------ |
| 革の帽子         | kawanobōshi      | Leather Cap     | Leather Hat    | Leather Cap        |
| 羽根つき帽子     | hanetsukibōshi   | Feathered Cap   | Feather Hat    | Plumed Hat         |
| 赤ずきん         | akazukin         | Red Cowl        | Red Hood       | Red Hood           |
| ヘッドギア       | heddogia         | Headgear        | Headgear       | Headgear           |
| 三角帽子         | sankakubōshi     | Tricorn Cap     | Triangle Hat   | Wizard's Hat       |
| グリーンベレー   | gurīnberē        | Green Beret     | Green Beret    | Green Beret        |
| ねじり鉢巻き     | nejirihachimaki  | Twist Headband  | Twist Headband | Headband           |
| 司祭の帽子       | shisainobōshi    | Priest's Cap    | Holy Miter     | Celebrant's Miter  |
| 黒ずきん         | kurozukin        | Black Cowl      | Black Hood     | Black Cowl         |
| 金の髪飾り       | kinnokamikazari  | Gold Hairpin    | Golden Hairpin | Gold Hairpin       |
| 閃光魔帽         | senkōmabō        | Foxfire Hat     | Flash Hat      | Lambent Hat        |
| シーフの帽子     | shīfunobōshi     | Thief's Cap     | Thief Hat      | Thief's Cap        |
| スタイルビット   | sutairubitto     | Style Bit       | (not present)  | Brass Coronet      |
| アカシアの帽子   | akashianobōshi   | Acacia Cap      | (not present)  | Acacia Hat         |
| レザーヘルム     | rezāherumu       | Leather Helm    | Leather Helmet | Leather Helm       |
| ブロンズヘルム   | buronzuherumu    | Bronze Helm     | Bronze Helmet  | Bronze Helm        |
| アイアンヘルム   | aianherumu       | Iron Helm       | Iron Helmet    | Iron Helm          |
| バルビュータ     | barubyūta        | Barbut          | Barbuta        | Barbut             |
| ミスリルヘルム   | misuriruherumu   | Mithril Helm    | Mythril Helmet | Mythril Helm       |
| ゴールドヘルム   | gōrudoherumu     | Gold Helm       | Gold Helmet    | Golden Helm        |
| クロスヘルム     | kurosuherumu     | Cross Helm      | Cross Helmet   | Close Helmet       |
| ダイアヘルム     | daiaherumu       | Diamond Helm    | Diamond Helmet | Diamond Helm       |
| プラチナヘルム   | purachinaherumu  | Platinum Helm   | Platina Helmet | Platinum Helm      |
| サークレット     | sākuretto        | Circlet         | Circlet        | Circlet            |
| クリスタルヘルム | kurisutaruherumu | Crystal Helm    | Crystal Helmet | Crystal Helm       |
| 源氏の兜         | genjinokabuto    | Genji Helmet    | Genji Helmet   | Genji Helm         |
| グランドヘルム   | gurandoherumu    | Grand Helm      | Grand Helmet   | Grand Helm         |
| バンガード       | bangādo          | Vanguard        | (not present)  | Vanguard Helm      |
| オニオンヘルム   | onionherumu      | Onion Helm      | (not present)  | Onion Helm         |
| カチューシャ     | kachūsha         | Cachusha        | Cachusha       | Cachusha           |
| バレッタ         | baretta          | Barette         | Barette        | Barette            |
| リボン           | ribon            | Ribbon          | Ribbon         | Ribbon             |

### Armor

| Japanese         |_ Romaji           | Síle            | PlayStation    | War of the Lions   |
| ---------------- | ----------------- | --------------- | -------------- | ------------------ |
| 服               | fuku              | Suit            | Clothes        | Clothing           |
| 革の服           | kawanofuku        | Leather Suit    | Leather Outfit | Leather Clothing   |
| レザープレイト   | rezāpureito       | Leather Plate   | Leather Vest   | Leather Plate      |
| チェインプレイト | cheinpureito      | Chain Plate     | Chain Vest     | Ringmail           |
| ミスリルベスト   | misurirubesuto    | Mithril Vest    | Mythril Vest   | Mythril Vest       |
| アダマンベスト   | adamanbesuto      | Adamantine Vest | Adaman Vest    | Adaman Vest        |
| 魔術師の服       | majutsushinofuku  | Mage's Suit     | Wizard Outfit  | Wizard Clothing    |
| ブリガンダイン   | burigandain       | Brigandine      | Brigandine     | Brigandine         |
| 柔術道着         | jūjutsudōgi       | Jujutsu Gi      | Judo Outfit    | Jujitsu Gi         |
| 力だすき         | chikaradasuki     | Power Sash      | Power Sleeve   | Power Garb         |
| 大地の衣         | daichinokoromo    | Earth Clothes   | Earth Clothes  | Gaia Gear          |
| 黒装束           | kuroshōzoku       | Black Costume   | Black Costume  | Black Garb         |
| 忍びの衣         | shinobinokoromo   | Shinobi Clothes | Secret Clothes | Ninja Gear         |
| ミラージュベスト | mirājubesuto      | Mirage Vest     | (not present)  | Mirage Vest        |
| ミネルバビスチェ | minerubabisuche   | Minerva Bustier | (not present)  | Minerva Bustier    |
| ラバーコンシャス | rabākonshasu      | Rubber Costume  | Rubber Costume | Rubber Suit        |
| ブレイブスーツ   | bureibusūtsu      | Brave Suit      | (not present)  | Brave Suit         |
| 麻のローブ       | asanorōbu         | Hemp Robe       | Linen Robe     | Hempen Robe        |
| シルクのローブ   | shirukunorōbu     | Silk Robe       | Silk Robe      | Silken Robe        |
| 魔術師のローブ   | majutsushinorōbu  | Mage's Robe     | Wizard Robe    | Wizard's Robe      |
| カメレオンローブ | kamereonrōbu      | Chameleon Robe  | Chameleon Robe | Chameleon Robe     |
| 白のローブ       | shironorōbu       | White Robe      | White Robe     | White Robe         |
| 黒のローブ       | kuronorōbu        | Black Robe      | Black Robe     | Black Robe         |
| 光のローブ       | hikarinorōbu      | Shimmering Robe | Light Robe     | Luminous Robe      |
| ローブオブロード | rōbuoburōdo       | Robe of Lords   | Robe of Lords  | Lordly Robe        |
| 賢者のローブ     | kenjanorōbu       | Sage's Robe     | (not present)  | Sage's Robe        |
| 革の鎧           | kawanoyoroi       | Leather Armor   | Leather Armor  | Leather Armor      |
| リネンキュラッサ | rinenkurassa      | Linen Cuirass   | Linen Cuirass  | Linen Cuirass      |
| ブロンズアーマー | buronzuāmā        | Bronze Armor    | Bronze Armor   | Bronze Armor       |
| チェインメイル   | cheinmeiru        | Chain Mail      | Chain Mail     | Chainmail          |
| ミスリルアーマー | misuriruāmā       | Mithril Armor   | Mythril Armor  | Mythril Armor      |
| プレイトメイル   | pureitomeiru      | Plate Mail      | Plate Mail     | Plate Mail         |
| ゴールドアーマー | gōrudoāmā         | Gold Armor      | Gold Armor     | Golden Armor       |
| ダイアアーマー   | daiaāmā           | Diamond Armor   | Diamond Armor  | Diamond Armor      |
| プラチナアーマー | purachinaāmā      | Platinum Armor  | Platina Armor  | Platinum Armor     |
| キャラビニエール | kyarabiniēru      | Carabineer      | Carabini Mail  | Carabineer Mail    |
| リフレクトメイル | rifurekutomeiru   | Reflect Mail    | Reflect Mail   | Mirror Mail        |
| クリスタルメイル | kurisutarumeiru   | Crystal Mail    | Crystal Armor  | Crystal Armor      |
| 源氏の鎧         | genjinoyoroi      | Genji Armor     | Genji Armor    | Genji Armor        |
| グランドアーマー | gurandoāmā        | Grand Armor     | (not present)  | Grand Armor        |
| マクシミリアン   | makushimirian     | Maximilian      | Maximillian    | Maximillian        |
| オニオンアーマー | onionāmā          | Onion Armor     | (not present)  | Onion Armor        |

### Shields

| Japanese         |_. Romaji         | Síle                | PlayStation    | War of the Lions |
| ---------------- | ---------------- | ------------------- | -------------- | ---------------- |
| エスカッション   | esukasshon       | Escutcheon          | Escutcheon     | Escutcheon       |
| イージスの盾     | ījisnotate       | Aegis Shield        | Aegis Shield   | Aegis Shield     |
| バックラー       | bakkurā          | Buckler             | Buckler        | Buckler          |
| ブロンズシールド | buronzushīrudo   | Bronze Shield       | Bronze Shield  | Bronze Shield    |
| ラウンドシールド | raundoshīrudo    | Round Shield        | Round Shield   | Round Shield     |
| ミスリルシールド | misurirushīrudo  | Mithril Shield      | Mythril Shield | Mythril Shield   |
| ゴールドシールド | gōrudoshīrudo    | Golden Shield       | Gold Shield    | Golden Shield    |
| アイスシールド   | aisushīrudo      | Ice Shield          | Ice Shield     | Ice Shield       |
| フレイムシールド | fureimushīrudo   | Flame Shield        | Flame Shield   | Flame Shield     |
| ダイアシールド   | daiashīrudo      | Diamond Shield      | Diamond Shield | Diamond Shield   |
| プラチナシールド | purachinashīrudo | Platinum Shield     | Platina Shield | Platinum Shield  |
| クリスタルの盾   | kurisutarunotate | Crystal Shield      | Crystal Shield | Crystal Shield   |
| 源氏の盾         | genjinotate      | Genji's Shield      | Genji Shield   | Genji Shield     |
| カエサルプレート | kaesarupurēto    | Caesar Plate        | Kaiser Plate   | Kaiser Shield    |
| ベネチアプレート | benuchiapurēto   | Venetian Plate      | Venetian Plate | Venetian Shield  |
| レバリーシールド | rebarīshīrudo    | Reverie Shield      | (not present)  | Reverie Shield   |
| エスカッション   | esukasshon       | Escutcheon          | Escutcheon     | Escutcheon (II)  |
| オニオンシールド | onionshīrudo     | Onion Shield        | (not present)  | Onion Shield     |

### Accessories

| Japanese         |_ Romaji          | Síle                | PlayStation    | War of the Lions   |
| ---------------- | ---------------- | ------------------- | -------------- | ------------------ |
| バトルブーツ     | batorubūtsu      | Battle Boots        | Battle Boots   | Battle Boots       |
| スパイクシューズ | supaikushūsu     | Spike Shoes         | Spike Shoes    | Spiked Boots       |
| ラバーシューズ   | rabāshūsu        | Rubber Shoes        | Rubber Shoes   | Rubber Boots       |
| フェザーブーツ   | fezābūtsu        | Feather Boots       | Feather Boots  | Winged Boots       |
| ゲルミナスブーツ | geruminasubūtsu  | Germinas Boots      | Germinas Boots | Germinas Boots     |
| エルメスの靴     | erumesunokutsu   | Hermes's Sandals    | Sprint Shoes   | Hermes Shoes       |
| 赤い靴           | akaikutsu        | Red Shoes           | Red Shoes      | Red Shoes          |
| ガイウスカリグ   | gaiusukarigu     | Gaius Caligae       | (not present)  | Gaius Caligae      |
| スモールマント   | sumōrumanto      | Small Mantle        | Small Mantle   | Shoulder Cape      |
| 革のマント       | kawanomanto      | Leather Mantle      | Leather Mantle | Leather Cloak      |
| 魔道士のマント   | madōshinomanto   | Mage's Mantle       | Wizard Mantle  | Mage's Cloak       |
| エルフのマント   | erufunomanto     | Elf's Mantle        | Elf Mantle     | Elven Cloak        |
| ドラキュラマント | dorakyuramanto   | Dracula's Mantle    | Dracula Mantle | Vampire Cape       |
| フェザーマント   | fezāmanto        | Feather Mantle      | Feather Mantle | Featherweave Cloak |
| 消えるマント     | kierumanto       | Invisibility Mantle | Vanish Mantle  | Invisibility Cloak |
| まもりの指輪     | mamorinoyubiwa   | Protect Ring        | Defense Ring   | Protect Ring       |
| 魔法のリング     | mahōnoringu      | Magical Ring        | Magic Ring     | Magick Ring        |
| リフレクトリング | rifurekutoringu  | Reflect Ring        | Reflect Ring   | Reflect Ring       |
| 天使の指輪       | tenshinoyubiwa   | Angel's Ring        | Angel Ring     | Angel Ring         |
| 呪いの指輪       | noroinoyubiwa    | Cursed Ring         | Cursed Ring    | Cursed Ring        |
| 賢者の指輪       | kenjanoyubiwa    | Sage's Ring         | (not present)  | Sage's Ring        |
| ダイアの腕輪     | daianoudewa      | Diamond Bangle      | Diamond Armlet | Diamond Bracelet   |
| まもりの腕輪     | mamorinoudewa    | Protect Bangle      | Defense Armlet | Guardian Bracelet  |
| ン・カイの腕輪   | n.kainoudewa     | N'Kai Bangle        | N-Kai Armlet   | Nu Khai Armband    |
| ヒスイの腕輪     | hisuinoudewa     | Jade Bangle         | Jade Armlet    | Jade Armlet        |
| 百八の数珠       | hyakuhachinojuzu | Prayer Beads        | 108 Gems       | Japa Mala          |
| 星天の腕輪       | seitennoudewa    | Empyreal Bangle     | (not present)  | Empyreal Armband   |
| パワーリスト     | pawārisuto       | Power Wrist         | Power Wrist    | Power Gauntlet     |
| 魔力の小手       | maryokunokote    | Charm Gauntlet      | Magic Gauntlet | Magepower Glove    |
| ブレイサー       | bureisā          | Bracer              | Bracer         | Bracer             |
| 源氏の小手       | genjinokote      | Genji Gauntlet      | Genji Gauntlet | Genji Glove        |
| 盗賊の小手       | tōzokunokote     | Thief's Gauntlet    | (not present)  | Brigand's Gloves   |
| オニオレット     | onioretto        | Onion Gauntlet      | (not present)  | Onion Gloves       |
| シェルシェ       | sherushe         | Cherche             | Cherche        | Cherche            |
| ソルティレージュ | sorutirēju       | Sortilége           | Salty Rage     | Sortilége          |
| シャンタージュ   | shantāju         | Chantage            | Chantage       | Chantage           |
| セッティエムソン | settiemuson      | Septiéme Son        | Setiemson      | Septième           |
| ティンカーリップ | tingārippu       | Tyngar Lip          | (not present)  | Tynar Rouge        |

## Monsters

| Japanese         | Romaji          | Síle             | PlayStation    | War of the Lions   |
| ---------------- | --------------- | ---------------- | -------------- | ------------------ |
| チョコボ         | chokobo         | Chocobo          | Chocobo        | Chocobo            |
| 黒チョコボ       | kurochokobo     | Black Chocobo    | Black Chocobo  | Black Chocobo      |
| 赤チョコボ       | akachokobo      | Red Chocobo      | Red Chocobo    | Red Chocobo        |
| ゴブリン         | goburin         | Goblin           | Goblin         | Goblin             |
| ブラックゴブリン | burakkugoburin  | Black Goblin     | Black Goblin   | Black Goblin       |
| ガルブデガック   | garubudegakku   | Garbdegack       | Gobbledeguck   | Gobbledyguck       |
| ボム             | bomu            | Bomb             | Bomb           | Bomb               |
| グレネイド       | gureneido       | Grenade          | Grenade        | Grenade            |
| イクスプロジャ   | ikusupuroja     | Exploder         | Explosive      | Exploder           |
| レッドパンサー   | reddopansā      | Red Panther      | Red Panther    | Red Panther        |
| クアール         | kuāru           | Coeurl           | Cuar           | Coeurl             |
| バンパイア       | banpaia         | Vampire          | Vampire        | Vampire Cat        |
| ピスコディーモン | pisukodīmon     | Piscodemon       | Pisco Demon    | Piscodaemon        |
| スクイドラーケン | sukuidorāken    | Squidraken       | Squidlarkin    | Squidraken         |
| マインドフレイア | maindofureia    | Mind Flayer      | Mindflare      | Mindflayer         |
| スケルトン       | sukeruton       | Skeleton         | Skeleton       | Skeleton           |
| ボーンスナッチ   | bōnsunacchi     | Bonesnatch       | Bone Snatch    | Bonesnatch         |
| リビングボーン   | ribingubōn      | Living Bone      | Living Bone    | Skeletal Fiend     |
| グール           | gūru            | Ghoul            | Ghoul          | Ghoul              |
| ガスト           | gasuto          | Ghast            | Gust           | Ghost              |
| レブナント       | rebunanto       | Revenant         | Revnant        | Revenant           |
| フロータイボール | furōtaibōru     | Floating Eyeball | Flotiball      | Floating Eye       |
| アーリマン       | āriman          | Ahriman          | Ahriman        | Ahriman            |
| プレイグ         | pureigu         | Plague           | Plague         | Plague Horror      |
| ジュラエイビス   | juraeibisu      | Jura Aevis       | Juravis        | Jura Aevis         |
| スチールホーク   | suchīruhōku     | Steel Hawk       | Steel Hawk     | Steelhawk          |
| コカトリス       | kokatorisu      | Cockatrice       | Cocatoris      | Cockatrice         |
| うりぼう         | uribō           | Pig              | Uribo          | Pig                |
| ポーキー         | pōkī            | Porky            | Porky          | Swine              |
| ワイルドボー     | wairudobō       | Wild Boar        | Wildbow        | Wild Boar          |
| ウッドマン       | uddoman         | Woodman          | Woodman        | Dryad              |
| トレント         | torento         | Treant           | Trent          | Treant             |
| タイジュ         | taiju           | Huge Tree        | Taiju          | Elder Treant       |
| 牛鬼             | ushioni         | Ox Demon         | Bull Demon     | Wisenkin           |
| ミノタウロス     | minotaurosu     | Minotaurus       | Minotaurus     | Minotaur           |
| セクレト         | sekureto        | Sekhret          | Sacred         | Sekhret            |
| モルボル         | moruboru        | Mortboule        | Morbol         | Malboro            |
| オチュー         | ochū            | Otyugh           | Ochu           | Ochu               |
| モルボルグレイト | moruborugureito | Great Mortboule  | Great Morbol   | Great Malboro      |
| ベヒーモス       | behīmosu        | Behemoth         | Behemoth       | Behemoth           |
| キングベヒーモス | kingubehīmosu   | King Behemoth    | King Behemoth  | Behemoth King      |
| ダークベヒーモス | dākubehīmosu    | Dark Behemoth    | Dark Behemoth  | Dark Behemoth      |
| ドラゴン         | doragon         | Dragon           | Dragon         | Dragon             |
| ブルードラゴン   | burūdoragon     | Blue Dragon      | Blue Dragon    | Blue Dragon        |
| レッドドラゴン   | reddodoragon    | Red Dragon       | Red Dragon     | Red Dragon         |
| ヒュドラ         | hyudora         | Hydra            | Hyudra         | Hydra              |
| ハイドラ         | haidora         | Hydra            | Hydra          | Great Hydra        |
| ティアマット     | tiamatto        | Tiamat           | Tiamat         | Tiamat             |
| 不浄王           | fujōō           | Unclean King     | Impure King    | The Impure         |
| 魔人             | majin           | Demon            | Warlock        | The Gigas          |
| 死の天使         | shinotenshi     | Angel of Death   | Angel of Death | The Death Seraph   |
| 憤怒の霊帝       | fundonoreitei   | Spirit of Wrath  | Ghost of Fury  | Wroth              |
| 統制者           | tōseisha        | Judiciar         | Regulator      | Bringer of Order   |
| 聖天使           | seitenshi       | Holy Angel       | Holy Angel     | The High Seraph    |
| 聖大天使         | seidaitenshi    | Holy Archangel   | Arch Angel     | Arch Seraph        |
| サーペンタリウス | sāpentariusu    | Serpentarius     | Serpentarius   | Serpentarius       |
| 暗雲             | anun            | Dark Cloud       | Dark Cloud     | The Dark           |
| アルケオデーモン | arukeodēmon     | Archaeodaemon    | Archaic Demon  | Archaeodaemon      |
| アルテマデーモン | arutemadēmon    | Ultima Daemon    | Ultima Demon   | Ultima Demon       |
| アパンダ         | apanda          | Apanta           | Apanda         | Reaver             |
| ビブロス         | biburosu        | Byblos           | Byblos         | Byblos             |
| 鉄巨人           | tetsukyojin     | Steel Giant      | Steel Giant    | Automaton          |
| ホーリードラゴン | horīdoragon     | Holy Dragon      | Holy Dragon    | Holy Dragon        |
| ダークドラゴン   | dākudoragon     | Dark Dragon      | (not present)  | Dark Dragon        |

## Locations

| Japanese             |_ Romaji                | Síle                     | PlayStation                   | War of the Lions         |
| -------------------- | ---------------------- | ------------------------ | ----------------------------- | ------------------------ |
| イグーロス城         | igūrosujō              | Castle Eagulos           | Igros Castle                  | Eagrose Castle           |
| 魔法都市ガリランド   | mahōtoshigarirando     | Magical City Gariland    | Magic City of Gariland        | Magick City of Gariland  |
| 貿易都市ドーター     | bōekitoshi dōta        | Commerce City Dorter     | Dorter Trade City             | Merchant City of Dorter  |
| ジークデン砦         | jīkudentei             | Fort Zealkden            | Fort Zeakden                  | Ziekden Fortress         |
| 盗賊の砦             | tōzokunotei            | Robber's Redoubt         | Thieves' Fort                 | Brigands' Den            |
| マンダリア平原       | mandariaheigen         | Mandaria Fields          | Mandalia Plains               | Mandalia Plains          |
| スウィージの森       | suwījinomori           | Sweege Woodlands         | Sweegy Woods                  | Siedge Weald             |
| レナリア台地         | renariadaichi          | Lenaria Mesa             | Lenalian Plateau              | Lenalian Plateau         |
| ライオネル城         | raionerujō             | Castle Lionel            | Lionel Castle                 | Lionel Castle            |
| 城塞都市ザランダ     | jōsaitoshizaranda      | Fortress City Zaranda    | Zaland Fort City              | Castled City of Zaland   |
| 貿易都市ウォージリス | bōekitoshiwōjirisu     | Commerce City Waljiris   | Warjilis Trade City           | Port City of Warjilis    |
| 機工都市ゴーグ       | kikōtoshi gōgu         | Mechanized City Golg     | Goug Machine City             | Clockwork City of Goug   |
| ゴルゴラルダ処刑場   | gorugorarudashōkeijō   | Golgralda Gallows        | Golgorand Execution Site      | Golgollada Gallows       |
| ツィゴリス湿原       | zigorisushūgen         | Zigris Marsh             | Zigolis Swamp                 | Tchigolith Fenlands      |
| バリアスの丘         | bariasunokyū           | Balias Hill              | Bariaus Hill                  | Balias Tor               |
| バリアスの谷         | bariasunokoku          | Balias Valley            | Bariaus Valley                | Balias Swale             |
| ランベリー城         | ranberījō              | Castle Lanberry          | Limberry Castle               | Limberry Castle          |
| ドルボダル湿原       | dorubodarushūgen       | Dorvodal Marsh           | Dolbodar Swamp                | Dorvauldar Marsh         |
| ベッド砂漠           | beddosabaku            | Vedd Desert              | Bed Desert                    | Beddha Sandwaste         |
| ポエスカス湖         | poesukasumizūmi        | Lake Poescus             | Lake Poescas                  | Lake Poescas             |
| ゼルテニア城         | zeruteniajō            | Castle Zeltennia         | Zeltennia Castle              | Zeltennia Castle         |
| 貿易都市ザーギドス   | bōekitoshizāgidosu     | Commerce City Zal Ghidos | Zarghidas Trade City          | Trade City of Sal Ghidos |
| フィナス河           | finasuka               | River Finnus             | Finath River                  | Finnath Creek            |
| ゲルミナス山岳       | geruminasusangaku      | Germinas Peak            | Germinas Peak                 | Mount Germinas           |
| リオファネス城       | riofanesujō            | Castle Leofannes         | Riovanes Castle               | Riovanes Castle          |
| 城塞都市ヤードー     | jōsaitoshi yādō        | Fortress City Yaldor     | Yardow Fort City              | Walled City of Yardrow   |
| ユーグォの森         | yūgwonomori            | Yulgwood                 | Yuguo Woods                   | Yuguewood                |
| フォボハム平原       | fobohamuheigen         | Fobham Fields            | Fovoham Plains                | Fovoham Windflats        |
| 王都ルザリア         | ōtoruzaria             | Imperial Capital Ruzalia | Lesalia Imperial Capital      | Royal City of Lesalia    |
| 炭鉱都市ゴルランド   | tankōtoshigorurando    | Coal City Gorland        | Goland Coal City              | Mining Town of Gollund   |
| ゴルランドの炭坑     | gorurandonotankō       | Gorland Coal Mine        | Colliery                      | Gollund Colliery         |
| ゴルランドの坑道     | gorurandonokōdō        | Gorland Mineshaft        | Underground Passage in Goland | Gollund Coal Shaft       |
| 自治都市ベルベニア   | jijitoshiberubenia     | City-State Verbenia      | Bervenia Free City            | Free City of Bervenia    |
| オーボンヌ修道院     | ōbonnushūdōin          | Orbonne Monastery        | Orbonne Monastery             | Orbonne Monastery        |
| 地下書庫             | chikashoko             | Library Cellar           | Underground Book Storage      | Monastery Vaults         |
| ベスラ要塞           | besurayōsai            | Bethra Bastion           | Bethla Garrison               | Fort Besselat            |
| ゼクラス砂漠         | zekurasusabaku         | Zeklus Desert            | Zeklaus Desert                | Zeklaus Desert           |
| ベルベニア活火山     | berubeniakakkazan      | Verbenia Volcano         | Bervenia Volcano              | Mount Bervenia           |
| アラグアイの森       | araguainomori          | Araguay Woodlands        | Araguay Woods                 | Araguay Woods            |
| グローグの丘         | gurōgunokyū            | Grolgh Hill              | Grog Hill                     | Grogh Heights            |
| ゼイレキレ川         | zeirekiresen           | Zeirekille Stream        | Zirekile Falls                | Zeirchele Falls          |
| ドグーラ岬           | dogūrakō               | Doguera Cape             | Dogoula Pass                  | Dugeura Pass             |
| 聖ミュロンド寺院     | seimyurondojiin        | St. Mulonde Temple       | St. Murond Temple             | Mullonde Cathedral       |
| ネルベスカ神殿       | nerubesukashinden      | Nerveska Temple          | Nelveska Temple               | Nelveska Temple          |
| ディープダンジョン   | dīpudanjon             | Deep Dungeon             | Deep Dungeon                  | Midlight's Deep          |
| 死都ミュロンド       | shitomyurondo          | Necromanse Mulonde       | Murond Death City             | Necrohol of Mullonde     |
| 失われた聖域         | ushinawaretaseiki      | Lost Holyland            | Lost Sacred Precincts         | Lost Halidom             |
| 飛空艇の墓場         | hikuteinohoshū         | Airship Graveyard        | Graveyard of Airships         | Airship Graveyard        |

## Action Ability Quotes
{{< fftq-header >}}
	{{< fftq name="Punch Rush" jpname="連続拳" japanese="熱き正義の燃えたぎる! 赤き血潮の拳がうなる!" romaji="atsuki seigi no moe tagiru! akaki chishio no kobushi ga unaru! renzokuken!" sheila="This blood-red fist seethes with the burning heat of justice!" >}}
	{{< fftq name="Wave Strike" jpname="波動撃" japanese="渦巻く怒りが熱くする! これが咆哮の臨界!" romaji="uzumaku ikari ga atsuki suru! kore ga hōkō no rin! hadōgeki!" sheila="Meet the howling heat of surging wrath!" >}}
	{{< fftq name="Earth Render" jpname="地裂斬" japanese="大地の怒りがこの腕を伝う! 防御あたわず! 疾風," romaji="daichi no ikari ga kono ude o tsutau!" sheila="You can't stop the flowing wrath of the land!" >}}
	{{< fftq name="Pin-Point Strike" jpname="秘孔拳" japanese="この指先に全身全霊を込めて! 地獄への引導!" romaji="kono yubisaki ni zenshin-zenrei o komite! jigoku e no intō! hikōken!" sheila="This sublime finger shall usher you into Hell!" >}}
	{{< fftq name="Cure" jpname="ケアル" japanese="清らかなる生命の風よ 失いし力とならん!" romaji="kiyoraka naru seimei no kaze yo ushinai shiryoku to naran! kearu!" sheila="The purifying wind of life shall restore your lost strength!" >}}
	{{< fftq name="Curera" jpname="ケアルラ" japanese="清らかなる生命の風よ, 天空に舞い邪悪なる傷を癒せ!" romaji="kiyoraka naru seimei no kaze yo, tenkū ni mai jāku naru kizu o iyase! kearura!" sheila="The purifying winds of life blow, the heavens shall dance upon the wicked and heal your wounds!" >}}
	{{< fftq name="Curega" jpname="ケアルガ" japanese="空の下なる我が手に, 祝福の風の恵みあらん!" romaji="sora no shita naru waga te ni, shukufuku no kaze no megumi aran! kearuga!" sheila="The wind's joyous blessing beneath the heavens!" >}}
	{{< fftq name="Cureja" jpname="ケアルジャ" japanese="波動に揺れる大気, その風の腕で傷つける命を癒せ!" romaji="hadō ni yureru taiki, sono kaze no ude de kizutsukeru inochi o iyase! kearuja!" sheila="Heal these wounds with the arms of the surging winds that shiver!" >}}
	{{< fftq name="Raise" jpname="レイズ" japanese="生命をもたらしたる精霊よ 今一度我等がもとに!" romaji="seimei o motarashi taru seirei yo imaichido wareragamoto ni" sheila="O Holy One who created life, return this spirit to us once more!" >}}
	{{< fftq name="Raisega" jpname="アレイズ" japanese="生命を司る精霊よ, 失われゆく魂に, 今一度生命を与えたまえ!" romaji="seimei o tsukasadoru seirei yo, ushinawareyuku tamashii ni, imaichido seimei o ataeta mae! areizu!" sheila="O Blessed Holy One who governs life, return to us the spirits of the lost once more!" >}}
	{{< fftq name="Reraise" jpname="リレイズ" japanese="大気に満ち, 木々を揺らす波動 生命の躍動を刻め!" romaji="taiki ni michi, kigi o yurasu hadō seimei no yakudō o kizame! rereizu!" sheila="The waves that fill the air and tremble the trees, preserve the dynamism of life!" >}}
	{{< fftq name="Regenerate" jpname="リジェネ" japanese="森羅万象の生命を宿すものたち 命分かち, 共に在らん!" romaji="shinra banshō no seimei o shukusu monotachi inochi wakachi, tomoni aran" sheila="The life that binds the universe and everything in it together flows through us all!" >}}
	{{< fftq name="Protect" jpname="プロテス" japanese="たゆとう光よ, 見えざる鎧となりて 小さき命を守れ･･･" romaji="toyutō hikari yo, miezaru yoroi to narite chiisaki inochi o mamore… purotesu!" sheila="O wavering light, be the armor that guards this young life…" >}}
	{{< fftq name="Protectga" jpname="プロテジャ" japanese="大気に散る光よ, その力解き放ち 堅牢なる鎧となれ" romaji="taiki ni chiru hikari yo, sono chikara tokihanachi kenrō naru yoroi to nare! puroteja!" sheila="O scattering light of the air, release thy sturdy strength and become our armor!" >}}
	{{< fftq name="Shell" jpname="シェル" japanese="沈黙の光よ, 音の波動のもたらす 邪悪な影から守りたまえ!" romaji="chinmoku no hikari yo, oto no hadō no motarasu jākuna kage kara mamori tamae! sheru!" sheila="O silent light, guard us against the wicked shadows of surging sound!" >}}
	{{< fftq name="Shellga" jpname="シェルジャ" japanese="揺らぎ無き光よ, 魔力の咆哮から 我らを守りたまえ!" romaji="yuragi naki hikari yo, maryoku no hōkō kara warera o mamori tamae! sheruja!" sheila="O unshaken light, guard us from howling sorceries!" >}}
	{{< fftq name="Wall" jpname="ウォール" japanese="大地に眠る古の光, 眠れるその力を 地上にもたらせ!" romaji="daichi ni nemuru inishie no hikari, nemureru sono chikara o chijō ni motarase!" sheila="Ancient light that slumbers in the land, bring forth thy slumbering power!" >}}
	{{< fftq name="Null Status" jpname="エスナ" japanese="天駆ける風, 力の根源へと我を導き そを与えたまえ!" romaji="tenka keru kaze, chikara no konban e to ware o michibikiso o ataeta mae! esuna!" sheila="Heavenly winds, guide me to the source of power!" >}}
	{{< fftq name="Holy" jpname="ホーリー" japanese="汚れ無き天空の光よ,血にまみれし 不浄を照らし出せ!" romaji="yogore naki tenkū no hikari yo, chi ni mamire shi fujō o terashi shusse! hōrī!" sheila="O unsullied light, cast thy shining visage upon the impure!" >}}
	{{< fftq name="Fire" jpname="ファイア" japanese="岩砕き, 骸崩す, 地に潜む者たち 集いて赤き炎となれ!" romaji="iwa kudaki, mukure kuzusu, chi ni hisomu monotachi sudaite akaki honō to nare! faia!" sheila="Shatter boulders, destroy shells, the dormant land shall sing of red flames!" >}}
	{{< fftq name="Firera" jpname="ファイラ" japanese="地の砂に眠りし火の力目覚め 緑なめる赤き舌となれ!" romaji="chi no suna ni nemuri shi hi no chikara mezame midori nameru akaki shita to nare! faira!" sheila="Awaken, slumbering earthen sand, and become the fiery crimson that licks green!" >}}
	{{< fftq name="Firega" jpname="ファイガ" japanese="地の底に眠る星の火よ, 古の眠り覚まし 裁きの手をかざせ!" romaji="chi no soko ni nemuru hoshi no hi yo, inishie no nemurizamashi sabaki no te o kazase! faiga!" sheila="O blazing star sleeping beneath the land, awaken from thy ancient slumber and cast judgement!" >}}
	{{< fftq name="Fireja" jpname="ファイジャ" japanese="地に閉ざされし, 内臓にたぎる火よ 人の罪を問え!" romaji="chi no tozasare shi, naisō ni tagiru hi yo shito no tsumi o toe! faija!" sheila="O fire sealed within the bowels of the earth, purge the impure!" >}}
	{{< fftq name="Thunder" jpname="サンダー" japanese="まばゆき光彩を刃となして 地を引き裂かん!" romaji="mabayuki kōsai o ha to nashitechi o hikisakan! sandā!" sheila="Sunder the earth with a blade of dazzling brilliance!" >}}
	{{< fftq name="Thundera" jpname="サンダラ" japanese="暗雲に迷える光よ, 我に集い その力解き放て!" romaji="anun ni mayoe ru hikari yo, ware ni tsudoi sono chikara tokihanate! sandara!" sheila="O light lost in dark clouds, release to me thy strength!" >}}
	{{< fftq name="Thunderga" jpname="サンダガ" japanese="天空を満たす光, 一条に集いて 神の裁きとなれ!" romaji="tenkū o minasu hikari, ichijō ni sudaite kami no sabaki to nare! sandaga!" sheila="Cast down thy light that floods the skies, Holy One, and sound thy judgement!" >}}
	{{< fftq name="Thunderja" jpname="サンダジャ" japanese="天と地の精霊達の怒りの全てを 今そこに刻め!" romaji="ten to chi no seireitachi no ikari no subete o ima soko ni kizame! sandaja!" sheila="Engrave herein the all-encompassing wrath of the spirits of Creation!" >}}
	{{< fftq name="Blizzard" jpname="ブリザド" japanese="闇に生まれし精霊の吐息の 凍てつく風の刃に散れ!" romaji="yami ni umareshi seirei no toiki no itetsuku kaze no ha ni chire! burizado!" sheila="The sigh of an endarkened spirit shatters upon the winds' cutting frost!" >}}
	{{< fftq name="Blizzardra" jpname="ブリザラ" japanese="虚空の風よ, 非情の手をもって 人の業を裁かん!" romaji="kokū no kaze yo, hijō no te o motte hito no gō o sabakan! burizara!" sheila="O winds of the void, judge with ruthless hands the deeds of humanity!" >}}
	{{< fftq name="Blizzardga" jpname="ブリザガ" japanese="無念の響き, 嘆きの風を凍らせて 忘却の真実を語れ･･･" romaji="munen no hibiki, nageki no kaze no kōrisete bōkyaku no shinjitsu o katare… burizaga!" sheila="The reverberations of regret, freeze the winds of lamentation and speak forth the truth of oblivion…" >}}
	{{< fftq name="Blizzardja" jpname="ブリザジャ" japanese="大気に潜む無尽の水, 光を天に還し 形なす静寂を現せ!" romaji="taiki ni hisomu mujin no mizu, hikari o ten ni kaeshi katara nasu shijima o arawase! burizaja!" sheila="O infinite water hidden in air, return light to the firmament and reveal the shape of silence!" >}}
	{{< fftq name="Poison" jpname="ポイズン" japanese="大地に染み渡る, 復讐の赤い血よ その使命を果たせ･･･" romaji="daichi ni shimiwataru, fukushū no akai ra yo sono shimei o hatase… poizun!" sheila="The crimson blood of revenge that penetrates the earth, commence thy task…" >}}
	{{< fftq name="Toad" jpname="トード" japanese="カ～エ～ル～の～ き～も～ち～!" romaji="kaeru no kimochi! tōdo!" sheila="Feel froggish!" >}}
	{{< fftq name="Death" jpname="デス" japanese="命に飢えた死神達よ,汝らに その者の身を委ねん･･･" romaji="inochi ni ueni shi kamitatsu yo, nanjira ni sono mono no mi o yudaren… desu!" sheila="O gods of death who hunger for life, I commend this soul to thee…" >}}
	{{< fftq name="Flare" jpname="フレア" japanese="滅びゆく肉体に暗黒神の名を刻め 始源の炎蘇らん!" romaji="horobiyuku nikutai ni ankokushin no na o kizame hajimegen no honō yomigaeran! furea!" sheila="The rebirth of the primordial flame shall be carved in to the rotting corpses of fell gods!" >}}
	{{< fftq name="Haste" jpname="ヘイスト" japanese="ひるがえりて来たれ, 幾重にも その身を刻め･･･" romaji="hirugaeriteki tare, ikuenimo sono mi o kizame… heisuto!" sheila="Come around again and and again, carving yourself in to many layers of time…" >}}
	{{< fftq name="Hastega" jpname="ヘイスジャ" japanese="時の流れよ, 我が身を包み込み 巨大な渦をなせ･･･" romaji="toki no nageru yo, wagami o tsutsumikomi kyodaina uzu o nase… heisuja!" sheila="O timestream, envelop me in a huge whirlpool…" >}}
	{{< fftq name="Slow" jpname="スロウ" japanese="時よ, 足を休め, 選ばれし者にのみ 恩恵を与えよ!" romaji="ji yo, ashi o yasume, erabareshimono ni nomi onkei o ataeyo! surō!" sheila="O Time, legs at ease, grace only thy chosen ones!" >}}
	{{< fftq name="Slowga" jpname="スロウジャ" japanese="天空の意志に従い, 真実の時を刻み 天命とならん!" romaji="tenkū no ishi ni shitagai, shinjitsu no toki o kizame tenmei to naran! surōja!" sheila="In accordance with the firmament's will, thou faceth time's truth and thy irresistable destiny!" >}}
	{{< fftq name="Stop" jpname="ストップ" japanese="時を知る精霊よ, 因果司る神の手から 我を隠したまえ･･･" romaji="toki o shiru seirei yo, inga tsukasadoru kami no te kara ware o kakushita mae… sutoppu!" sheila="Holy One who knows time, veil me from the gods of causality…" >}}
	{{< fftq name="Immobilize" jpname="ドンムブ" japanese="命ささえる大地よ, 我を庇護したまえ 止めおけ!" romaji="inochi sasaeru daichi yo, ware o higo shitamae tomeoke! donmubu!" sheila="O earth that sustains life, ensnare my foe!" >}}
	{{< fftq name="Levitate" jpname="レビテト" japanese="慈悲に満ちた大地よ, つなぎとめる 手を緩めたまえ･･･" romaji="jihi michitani daichi yo, tsunagitomeru te o yurumeta mae… rebiteto!" sheila="O benevolent Earth, loosen thy hands…" >}}
	{{< fftq name="Reflect" jpname="リフレク" japanese="静寂に消えた無尽の言葉の骸達 闇を返す光となれ!" romaji="seijaku ni kieta mujin no kotoba no mukurotachi yami o kaesu hikari to nare! refureku!" sheila="Infinite words of books that vanished, become the light that returns darkness!" >}}
	{{< fftq name="Quick" jpname="クイック" japanese="心震え, 失われた時間の輝石を 螺旋の相に還さん!" romaji="kokoro furue, ushinawareta jikan no kiseki o rasen no sō ni kaesan! kuikku!" sheila="Trembling spirit, return through the spiraling shards of lost time!" >}}
	{{< fftq name="Gravity" jpname="グラビデ" japanese="無念の死を抱き続ける大地よ, 黒き呪縛となれ･･･" romaji="munen no shi o idamatsuzukeru daichi yo, kuroki jubaku to nare… gurabide!" sheila="The Earth bearing Death's regrets binds thee in blackness…" >}}
	{{< fftq name="Gravityga" jpname="グラビガ" japanese="魔空の時に生まれた黒き羊よ 現世の光を包め･･･" romaji="masora no tokini umarete kuroki hitsuji yo gense no hikari o kurume… gurabiga!" sheila="Black sheep borne of demonic sky, embrace the light of this world…" >}}
	{{< fftq name="Meteor" jpname="メテオ" japanese="時は来た. 許されざる者達の頭上に 星砕け降り注げ!" romaji="toki wa kita. yurusarezarumonotachi no zujō ni hoshi kudake furisosoge! meteo!" sheila="The time has come… The shattered stars above shall rain upon thee!" >}}
	{{< fftq name="Mógli" jpname="モーグリ" japanese="モグのおまじない「クポーー！ くるくるぴゅ～･･･" romaji="mogu no omajinai [kupo—! kurukurupyu~… mōguri!]" sheila="“Mog's Charm” Kupo~! (spinning whoosh)" >}}
	{{< fftq name="Shiva" jpname="シヴァ" japanese="氷河の結晶「風、光の波動の静寂に消える時 我が力とならん･･･" romaji="hyōga no kesshō [kaze, hikari no hadō no seijaku ni kaeru toki waga chikara to naran… shiva!]" sheila="“Crystal Glacier” O silent surging light and wind, become my irresistable might…" >}}
	{{< fftq name="Ramuh" jpname="ラムウ" japanese="裁きのいかずち「森羅万象の翁 汝の審判を仰ぐ!" romaji="sabaki no ikazuchi [shinra banshō no okina nanji no shinban o aogu! ramū!]" sheila="“Judgement Bolt” Gaze heavenward as the Father of Creation casts judgement upon thee!" >}}
	{{< fftq name="Ifrit" jpname="イフリート" japanese="灼熱の地獄「創世の火を胸に抱く灼熱の王 灰塵に化せ!" romaji="shakunetsu no jigoku [sōsei no hi o mune ni idaku shakunetsu no ō kaijin ni kase! ifurīto!]" sheila="“Hellfire” Creation's Fire burns in thy breast, o fiery king! Burn it all to ashes…" >}}
	{{< fftq name="Titan" jpname="タイタン" japanese="大地の怒り「大地を統べる無限の躍動を以って, 圧殺せん!" romaji="daichi no ikari [daichi no suberu mugen no yakudō o motte, assatsusen! taitan!]" sheila="“Earthen Wrath” Crush them all with the boundless energy that governs the earth!" >}}
	{{< fftq name="Golem" jpname="ゴーレム" japanese="アースウォール「大地を肉体とする堅牢なる肉体よ 我らを守らん!" romaji="āsuwōru [daichi o nikutai to suru kenrō nare nikutai yo warera o mamoran! gōremu!]" sheila="“Earth Wall” O Earth, become the stout wall that protects us!" >}}
	{{< fftq name="Carbuncle" jpname="カーバンクル" japanese="ルビーの光「紅石に眠りし瞳, 精霊の声に目覚めん 我が聖戦に光を!" romaji="rubī no hikari [akaishi ni nemuri shi hitomi, seirei no koe ni mezamen waga seisen ni hikari o! kābankuru!]" sheila="“Ruby Light” O sleeping ruby eye, awaken thy voice and unleash thine holy light of war!" >}}
	{{< fftq name="Bahamut" jpname="バハムート" japanese="メガフレア「夜闇の翼の竜よ, 怒れしば我と共に 胸中に眠る星の火を!" romaji="megafurea [yamen no tsubaki no ryū yo, okore shiba wareto tomoni kyūchū ni nemuru hoshi no hi o! bahamūto!]" sheila="“Megaflare” Thy dragon-wings enshroud us in night, unleash the wrath that is slumbering starfire in thy breast!" >}}
	{{< fftq name="Odin" jpname="オーディン" japanese="破壊の閃光「漆黒の光閃き, 大気の震えとなれ 斬鉄剣!" romaji="hakai no senkō [shikkoku no hikari hirameki, taiki no furue to nare zantetsuken! ōdin!]" sheila="“Flash of Destruction” With the flashing of darkest light, the very air trembles due to Zantetsuken!" >}}
	{{< fftq name="Leviathan" jpname="リヴァイアサン" japanese="ダイダルウェイブ「青き水の牙, 青き鎧を打ち鳴らして 汚れ清めたまえ!" romaji="taidaru weibu [aoki mizu no kiba, aoki yoroi o uchi narashite yogore kiyometa mae! rivaiasan!]" sheila="“Tidal Wave” Azure waves' fang and azure armor, strike a ringing blow to purify the unclean!" >}}
	{{< fftq name="Salamander" jpname="サラマンダー" japanese="火炎竜立ち昇らん「炎の精霊よ, 今一瞬の全ての炎を その手に委ねる･･･" romaji="kaen ryūdachi noboran [honō no seirei yo, ima isshun no subete no honō o sono te ni yuganeru… saramandā!]" sheila="“Dragons' Flame Ascendant“ O blazing spirit, entrust the flames of Creation to me…" >}}
	{{< fftq name="Sylph" jpname="シルフ" japanese="風のささやき「生々流転なす生命の源流に 我を導け!" romaji="kaze no sasayaki [seiseiruten nasu seimei no genryū ni ware o michibike! shirufu!]" sheila="“Whispering Wind” Guide me, o source of the Great Wheel of Life!" >}}
	{{< fftq name="Faerie" jpname="フェアリー" japanese="妖精の輝き「水晶に砕けた陽光のすべてを その薄羽に捧げる･･･" romaji="yōsei no kagayaki [suishō ni kudaketa yōkō no subete o sono usuba ni sasageru… fearī!]" sheila="“Faerie's Brilliance” Thy thin wings are the shattered crystals of sunlight…" >}}
	{{< fftq name="Lich" jpname="リッチ" japanese="不条理の翼「骸の手の死を歌い, 恐怖の戸を叩き 死の川を渡れ･･･" romaji="fujōri no tsubasa [mukuro no te no shi o utai, kyōfu no to o tataki shi no kawa o watare… ricchi!]" sheila="“Absurdity's Wing” Sing of Death's skeletal hands, rap upon Fear's door, and cross Death's river…" >}}
	{{< fftq name="Cyclops" jpname="クリュプス" japanese="恐怖の最終章「陽光閉ざす冷気に, 大気は刃となり 骸に刻まん!" romaji="kyōfu no saishūshō [yōkō tozasu reiki ni, taiki wa ha to nari mukuro ni kizaman! kuryupusu!]" sheila="“Final Chapter's Terror” Arctic air seals away sunlight and becomes a blade that slices even death!" >}}
	{{< fftq name="Zodiarc" jpname="ゾディアーク" japanese="暗闇の雲「絶対なる原理を知らしめたまえ 偉大なる戒律の王･･･" romaji="kurayami no kumo [zettai naru genri wo shirashimeta mae idai naru kairitsu no ō… zodiāku!]" sheila="“Cloud of Darkness” Bestow knowledge of Absolute Truth, O Blessed Holy One who has given us thy Commandments…" >}}
	{{< fftq name="Darkness-Binding Sign" jpname="闇縛符" japanese="光の全ては地に落ち, 全ては幻 意識の闇に沈め･･･" romaji="hikari no subete wa chi ni ochi, subete wa maboroshi ishiki no yami ni shizume… yamibakufu!" sheila="All light falls upon the land, all is an illusion that shall be plunged into darkness…" >}}
	{{< fftq name="Spell-Absorbing Chant" jpname="魔吸唱" japanese="魔の理を背負いし全て, 我がもとへ 光となりて集え･･･" romaji="ma no ri o seori shi subete, waga moto e hikari yo narite tsudoe… makyūshō!" sheila="O principles of magic, gather to me and become light…" >}}
	{{< fftq name="Life-Absorbing Chant" jpname="命吸唱" japanese="魔の理に従い、鼓動のいくつかを 我が身のために･･･" romaji="ma n o ri ni shitagai, kodō no ikutsu ka waga minotameni… inochikyūshō!" sheila="As per Section 3, Subsection A of the Laws of Magic, thou art required to surrender a portion of thy life-force to me…" >}}
{{< fftq-footer >}}
