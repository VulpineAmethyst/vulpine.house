---
title: Lufia II
translation: true
draft: true
---

## Items

### Weapons

| ラックレイピア   | rakkurepia      | Luck Rapier        |
| ラックブレード   | rakkuburēdo     | Luck Blade         |
| キラーソード     | kirāsōdo        | Killer Sword       |
| フライパン       | furaipan        | Frying Pan         |
| ほうちょう       | hōchō           | Kitchen Knife      |
| ナイフ           | naifu           | Knife              |
| レイピア         | repia           | Rapier             |
| バトルナイフ     | batorunaifu     | Combat Knife       |
| ブラッドソード   | buraddosōdo     | Blood Sword        |
| インセクトキラー | insekutokirā    | Insect Killer      |
| ダガー           | dagā            | Dagger             |
| ひかりのナイフ   | hikarinonaifu   | Light Knife        |
| ロングナイフ     | rongunaifu      | Long Knife         |
| いかりのナイフ   | ikarinonaifu    | Wrath Knife        |
| ショートソード   | shōtosōdo       | Shortsword         |
| ファイアーダガー | faiādagā        | Fire Dagger        |
| サーカスタム１１ | sākasutamu11    | Sarkastam 11       |
| クックリ         | kukkuri         | Kukri              |
| グラディウス     | guradiusu       | Gladius            |
| コールドレイピア | kōrudorepia     | Cold Rapier        |
| マルチソード     | maruchisōdo     | Multi-Sword        |
| シミター         | shimitā         | Scimitar           |
| ブロンズソード   | buronzusōdo     | Bronze Sword       |
| バトルレイピア   | batorurepia     | Combat Rapier      |
| ロングソード     | rongusōdo       | Longsword          |
| ロックブレイカー | rokkubureikā    | Rockbreaker        |
| バーニングソード | bāningusōdo     | Burning Sword      |
| ブロードソード   | burōdosōdo      | Broadsword         |
| デッカーソード   | dekkāsōdo       | Dekar Sword        |
| エストック       | esutokku        | Estoc              |
| バーサクブレード | bāsakuburēdo    | Berserk Blade      |
| ガデスのつるぎ   | gadesnotsurugi  | Gades's Sword      |
| シルバーレイピア | shirubārepia    | Silver Rapier      |
| フライスレイヤー | furaisureiyā    | Flyslayer          |
| アクアソード     | akuasōdo        | Aqua Sword         |
| マッドエッジ     | maddoejji       | Mad Edge           |
| レッドサーベル   | reddosāberu     | Red Saber          |
| ミストレイピア   | misutorepia     | Mist Rapier        |
| フリーズソード   | furīzusōdo      | Freeze Sword       |
| サンダーソード   | sandāsōdo       | Thunder Sword      |
| シルバーソード   | shirubāsōdo     | Silver Sword       |
| めがみのつるぎ   | megaminotsurugi | Goddess's Sword    |
| バスターソード   | basutāsōdo      | Buster Sword       |
| ドラグスレイヤー | doragusureiyā   | Dragonslayer       |
| ルーンレイピア   | rūnrepia        | Runed Rapier       |
| ゆきおんなのけん | yukionnanoken   | Ice Maiden Sword   |
| ジルコンソード   | jirukonsōdo     | Zirconium Sword    |
| たいこのつるぎ   | taikonotsurugi  | Competition Sword  |
| れっかのつるぎ   | rekkanotsurugi  | Flaming Sword      |
| こうりんのつるぎ | kōrinnotsurugi  | Halo Sword         |
| らいじゅうのけん | raijūnoken      | Thunderbeast Sword |
| ガイアスソード   | gaiasusōdo      | Gaius's Sword      |
| デュアルブレード | duaruburēdo     | Dual Blade         |
| こだいのつるぎ   | kodainotsurugi  | Ancient Sword      |
| ドラゴンブレード | doragonburēdo   | Dragon Blade       |
| バニーのけん     | banīnoken       | Bunny Blade        |
| たまごのけん     | tamagonoken     | Egg Sword          |
| フランシスカ     | furanshisuka    | Francisca          |
| サンダーアックス | sandāakkusu     | Thunder Axe        |
| ハンドアックス   | handoakkusu     | Hand Axe           |
| ブロンズアックス | buronzuakkusu   | Bronze Axe         |
| フライアックス   | furaiakkusu     | Fly Axe            |
| レイニーアックス | renīakkusu      | Rainy Axe          |
| グレートアックス | gurētoakkusu    | Great Axe          |
| ジルコンアックス | jirukonakkusu   | Zirconium Axe      |
| こんごうのおの   | kongōnoono      | Vajra Axe          |
| ブラッドロッド   | buraddoroddo    | Blood Rod          |
| メイス           | meisu           | Mace               |
| ロッド           | roddo           | Rod                |
| スタッフ         | sutaffu         | Staff              |
| スリープロッド   | surīpuroddo     | Sleep Rod          |
| ロングスタッフ   | rongusutaffu    | Long Staff         |
| モーニングスター | mōningusutā     | Morning Star       |
| ホーリースタッフ | hōrīsutaffu     | Holy Staff         |
| ハンマーロッド   | hanmāroddo      | Hammer Rod         |
| クリスタルワンド | kurisutaruwando | Crystal Wand       |
| シルバーロッド   | shirubāroddo    | Silver Rod         |
| ふしちょうのつえ | fushichōnotsue  | Phoenix Staff      |
| ジルコンロッド   | jirukonroddo    | Zirconium Rod      |
| ジルコンフレイル | jirukonfureiru  | Zirconium Flail    |
| ウイップ         | uippu           | Whip               |
| バトルワイヤー   | batoruwaiyā     | Combat Wire        |
| チェーン         | chēn            | Chain              |
| アクアウィップ   | akuauippu       | Aqua Whip          |
| カッターウイップ | kattāuippu      | Cutting Whip       |
| クイーンウイップ | kuīnuippu       | Queen's Whip       |
| こうふくのむち   | kōfukunomuchi   | Yielding Whip      |
| ホーリーウィップ | hōrīuippu       | Holy Whip          |
| ジルコンウイップ | jirukonuippu    | Zirconium Whip     |
| てんくうのむち   | tenkūnomuchi    | Visionary Whip     |
| デスピック       | desupikku       | Death Pick         |
| スピア           | supia           | Spear              |
| トライデント     | toraidento      | Trident            |
| ハルバート       | harubāto        | Halberd            |
| ヘビーランス     | hebīransu       | Heavy Lance        |
| すいりゅうのやり | suiryūnoyari    | Water Dragon Spear |
| すいこのやり     | suikonoyari     | Easy Spear         |
| バイスプライヤー | baisupuraiyā    | Vice Pliers        |
| スレイスピーカー | sureisupīkā     | Slayspeaker        |
| プラスドライバー | purasudoraibā   | Plas Driver        |
| フィグオルゴール | figuorugōru     | Figorgoal          |
| スタンガン       | sutangan        | Stungun            |
| バトルドライバー | batorudoraibā   | Combat Driver      |
| のろわれたゆみ   | norowaretayumi  | Cursed Bow         |
| アローランチャー | arōranchā       | Arrow-Launcher     |
| フリーズボウ     | furīzubō        | Freeze Bow         |
| アーティのゆみ   | ātinoyumi       | Arty's Bow         |

### Armor

| シースルーケープ | shīsurūkēpu      | Sheer Cape            |
| シースルーシルク | shīsurūshiruku   | Sheer Silk            |
| エプロン         | epuron           | Apron                 |
| ワンピース       | wanpīsu          | One-Piece             |
| クロス           | kurosu           | Cloth                 |
| はくい           | hakui            | White Robe            |
| レザーアーマー   | rezāāmā          | Leather Armor         |
| ドレス           | doresu           | Dress                 |
| ローブ           | rōbu             | Robe                  |
| コート           | kōto             | Coat                  |
| クロスアーマー   | kurosuāmā        | Cloth Armor           |
| ハードレザー     | hādorezā         | Hard Leather          |
| バニーのふく     | banīnofuku       | Bunny Outfit          |
| ライトドレス     | raitodoresu      | Light Dress           |
| ライトアーマー   | raitoāmā         | Light Armor           |
| ペプロス         | pepurosu         | Peplos                |
| カミュのよろい   | kamyunoyoroi     | Camu's Armor          |
| タイトドレス     | taitodoresu      | Tight Dress           |
| てんしのはごろも | tenshinohagoromo | Angel's Raiment       |
| チェインメイル   | cheinmeiru       | Chainmail             |
| アイアンメイル   | aianmeiru        | Iron Mail             |
| トーガ           | tōga             | Toga                  |
| チェインクロス   | cheinkurosu      | Chain Cloth           |
| ヘビークロス     | hebīkurosu       | Heavy Cloth           |
| ロックブレスト   | rokkuburesuto    | Rock Breastplate      |
| コトアルディ     | kotoarudi        | Cotoaldi              |
| プレートクロス   | purētokurosu     | Cloth Plate           |
| プレートブレスト | purētoburesuto   | Breastplate           |
| キラーアーマー   | kīrāāmā          | Killer Armor          |
| マジカルビキニ   | majikarubikini   | Magical Bikini        |
| シルクトーガ     | shirukutōga      | Silk Toga             |
| シルバーアーマー | shirubāāmā       | Silver Armor          |
| メタルメイル     | metarumeiru      | Metal Mail            |
| ライトジャケット | raitojaketto     | Light Jacket          |
| メタルコート     | metarukōto       | Metal Coat            |
| シルバーメイル   | shirubāmeiru     | Silver Mail           |
| ハードジャケット | hādojaketto      | Hard Jacket           |
| メタルアーマー   | metaruāmā        | Metal Armor           |
| キルテッドシルク | kiruteddoshiruku | Kilted Silk           |
| ハードケープ     | hādokēpu         | Hard Cape             |
| シルバーローブ   | shirubārōbu      | Silver Robe           |
| チャイナドレス   | chainadoresu     | China Dress           |
| プレートアーマー | purētoāmā        | Plate Armor           |
| リカバーアーマー | rikabāāmā        | Recovery Armor        |
| シルクローブ     | shirukurōbu      | Silk Robe             |
| プラチナプレート | purachinapurēto  | Platinum Plate        |
| ダイヤのはくい   | daiyanoyoroi     | Diamond Armor         |
| クリスタルローブ | kursutarurōbu    | Crystal Robe          |
| クリスタルメイル | kurisutarumeiru  | Crystal Mail          |
| ふっかつのよろい | fukkatsunoyoroi  | Rebirth Armor         |
| マジックスケイル | majikkusukeiru   | Magic Scale           |
| たましいのころも | tamashīnokoromo  | Soul Garb             |
| メタルジャケット | metarujaketto    | Metal Jacket          |
| エリナイトドレス | erinaitodoresu   | Erinite Dress         |
| ちからのローブ   | chikaranorōbu    | Power Robe            |
| ひかりのよろい   | hikarinoyoroi    | Shimmering Armor      |
| たいこのよろい   | taikonoyoroi     | Competition Armor     |
| ミラクルプレート | mirakurupurēto   | Miracle Plate         |
| プリンセスドレス | purinsesudoresu  | Princess Dress        |
| ゴッドローブ     | goddorōbu        | Godly Robe            |
| フルメタルメイル | furumetarumeiru  | Fullmetal Mail        |
| ルサイナアーマー | rusainaāmā       | Russaina Armor        |
| しんじゅのよろい | shinjunoyoroi    | Pearl Armor           |
| ジルコンブレスト | jirkonburesuto   | Zirconium Breastplate |
| ジルコンアーマー | jirukonāmā       | Zirconium Armor       |

### Shields

| まないた         | manaite          | Cutting Board      |
| スモールシールド | sumōrushīrudo    | Small Shield       |
| レザーシールド   | rezāshīrudo      | Leather Shield     |
| バックラー       | bakkurā          | Buckler            |
| バニーのおぼん   | banīnoobon       | Bunny Tray         |
| プチシールド     | puchishīrudo     | Petit Shield       |
| せいれいのうでわ | seireinoudewa    | Spirit Bangle      |
| ブレスレット     | buresuretto      | Bracelet           |
| ウッドシールド   | uddoshīrudo      | Wood Shield        |
| しんじゅのうでわ | shinjunoudewa    | Pearl Bangle       |
| ハードグローブ   | hādogurōbu       | Hard Glove         |
| カイトシールド   | kaitoshīrudo     | Kite Shield        |
| まもりのたて     | mamorinotate     | Guardian Shield    |
| ブロンズシールド | buronzushīrudo   | Bronze Shield      |
| いかりのうでわ   | ikarinoudewa     | Anger Bangle       |
| テクトグローブ   | tekutogurōbu     | Protective Glove   |
| ラウンドシールド | raundoshīrudo    | Round Shield       |
| ラージシールド   | rājishīrudo      | Large Shield       |
| フェザーシールド | fezāshīrudo      | Feather Shield     |
| タワーシールド   | tawāshīrudo      | Tower Shield       |
| シルバーシールド | shirubāshīrudo   | Silver Shield      |
| スパイクシールド | supaikushīrudo   | Spiked Shield      |
| カッターシールド | kattāshīrudo     | Cutter Shield      |
| ハードバックラー | hādobakkurā      | Hard Buckler       |
| メイジシールド   | meijishīrudo     | Mage Shield        |
| テクトバックラー | tekutobakkurā    | Protective Buckler |
| ゴールドグローブ | gōrudogurōbu     | Golden Glove       |
| ゴールドシールド | gōrudoshīrudo    | Golden Shield      |
| プラチナグローブ | purachinagurōbu  | Platinum Glove     |
| プラチナシールド | purachinashīrudo | Platinum Shield    |
| ガントレット     | gantoretto       | Gauntlet           |
| みずのこて       | mizunotate       | Water Shield       |
| ルーングローブ   | rūngurōbu        | Rune Glove         |
| ホーリーシールド | hōrīshīrudo      | Holy Shield        |
| ジルコングローブ | jirukongurōbu    | Zirconium Glove    |
| たいこのたて     | taikonotate      | Competition Shield |
| ほのおのたて     | honoonotate      | Flame Shield       |
| すいしょうのたて | suishōnotate     | Crystal Shield     |
| ボルトシールド   | borutoshīrudo    | Bolt Shield        |
| アポロンシールド | aporonshīrudo    | Apollon's Shield   |
| やみのかがみ     | yaminokagami     | Dark Mirror        |
| ジルコンシールド | jirukonshīrudo   | Zirconium Shield   |
| こんごうのたて   | kondōnotate      | Vajra Shield       |
| しんじゅのたて   | shinjunotate     | Pearl Shield       |

